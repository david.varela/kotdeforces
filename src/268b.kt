fun main() {
    fun readInt() = readLine()!!.toInt()

    var n = readInt() + 1
    var selecting = 1
    var result = 0
    while (--n > 0) {
        result += 1 + selecting * n - selecting
        selecting++
    }
    print(result)
}