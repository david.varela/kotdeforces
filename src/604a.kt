import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val points = intArrayOf(500, 1000, 1500, 2000, 2500)
    val times = readInts()
    val wrongs = readInts()
    val (hacks, nonHacks) = readInts()
    var sol = 0.0
    for (pos in 0 until 5)
        sol += max(0.3 * points[pos], (1 - times[pos] / 250.0) * points[pos] - 50 * wrongs[pos])
    sol += 100 * hacks - 50 * nonHacks
    print(sol.toInt())
}