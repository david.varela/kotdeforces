import java.math.BigInteger

fun main() {
    val n = BigInteger(readLine()!!)
    val modResult = n.mod(BigInteger.valueOf(4)).toInt()
    val two = intArrayOf(1, 2, 4, 3)
    val three = intArrayOf(1, 3, 4, 2)
    val four = intArrayOf(1, 4, 1, 4)
    print((1 + two[modResult] + three[modResult] + four[modResult]) % 5)
}