fun main() {
    val vowels = setOf('a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y')
    val s = readLine()!!.reversed()
    for (c in s)
        return if (c == ' ') continue
        else if (c == '?') continue
        else if (c in vowels) print("YES")
        else print("NO")
}