fun main() {
    fun readInt() = readLine()!!.toInt()

    val length = readInt()
    val speed1 = readInt()
    val speed2 = readInt()
    print(length.toFloat() / (speed1 + speed2) * speed1)

}