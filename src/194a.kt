import kotlin.math.max

fun main() {
    val (numExams, requiredSum) = readLine()!!.split(" ").map(String::toInt)
    if (requiredSum <= 2* numExams) return print(numExams)
    print(max(0, 3 * numExams - requiredSum))
}