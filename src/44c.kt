fun main() {
    val (numDays, numPeople) = readLine()!!.split(" ").map(String::toInt)
    val days = IntArray(numDays + 1) { 0 }
    for (i in 1..numPeople) {
        var (start, end) = readLine()!!.split(" ").map(String::toInt)
        while (start <= end) {
            days[start]++
            start++
        }
    }
    for (i in 1..numDays) {
        if (days[i] != 1) {
            print("$i ${days[i]}")
            return
        }
    }
    print("OK")
}