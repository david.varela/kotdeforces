fun main() {
    val s = readLine()!!
    var current = 'a'
    var count = 0
    var sol = 0
    for (c in s) {
        if (c == current && count < 5) count++
        else {
            sol++
            current = c
            count = 1
        }
    }
    print(sol)
}