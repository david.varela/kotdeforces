import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    fun sieve(limit: Int): List<Int> {
        val notPrimes = mutableSetOf<Int>()
        val primes = ArrayList<Int>()
        for (num in 2..limit) {
            if (num !in notPrimes) {
                primes.add(num)
                var notPrime = num + num
                while (notPrime <= limit) {
                    notPrimes.add(notPrime)
                    notPrime += num
                }
            }
        }
        return primes
    }

    fun List<Int>.next(num: Int): Int {
        var left = 0
        var right = this.size - 1
        loop@ while (left < right) {
            val middle = left + (right - left) / 2
            when {
                this[middle] == num -> {
                    left = middle
                    break@loop
                }
                this[middle] < num -> left = middle + 1
                else -> right = middle
            }
        }
        return this[left] - num
    }

    val (numRows, numColumns) = readInts()
    val matrix = Array(numRows) { IntArray(numColumns) }
    for (row in 0 until numRows)
        matrix[row] = readInts().toIntArray()
    val maximum = matrix.map { row -> row.maxOrNull()!! }.maxOrNull()!!
    val primes = sieve(2 * maximum)
    val map = mutableMapOf<Int, Int>()
    for (row in 0 until numRows)
        for (column in 0 until numColumns) {
            if (matrix[row][column] !in map) map[matrix[row][column]] = primes.next(matrix[row][column])
            matrix[row][column] = map[matrix[row][column]]!!
        }
    var sol = Int.MAX_VALUE
    for (row in matrix) sol = min(sol, row.sum())
    for (column in 0 until numColumns) sol = min(sol, matrix.sumBy { row -> row[column] })
    print(sol)
}
