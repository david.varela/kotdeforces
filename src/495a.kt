fun main() {
    val digitToOptions = listOf(2, 7, 2, 3, 3, 4, 2, 5, 1, 2)
//    print(readLine()!!.fold(1) { acc, c -> acc * digitToOptions[c - '0'] }) // A shorter alternative, less readable
    var sol = 1
    for (c in readLine()!!) sol *= digitToOptions[c - '0']
    print(sol)
}