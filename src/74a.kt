fun main() {
    fun readInt() = readLine()!!.toInt()

    val numContestants = readInt()
    var leaderHandle = ""
    var leaderPoints = Int.MIN_VALUE
    repeat(numContestants) {
        val info = readLine()!!.split(" ")
        val handle = info[0]
        val points = 100 * info[1].toInt() - 50 * info[2].toInt() + info[3].toInt() + info[4].toInt() +
                info[5].toInt() + info[6].toInt() + info[7].toInt()
        if (points > leaderPoints) {
            leaderPoints = points
            leaderHandle = handle
        }
    }
    print(leaderHandle)
}