import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    val numDays = readInt()
    val amountsToPrices = Array(numDays) { mutableListOf<Long>() }
    for (day in 0 until numDays)
        amountsToPrices[day] = readLongs() as MutableList<Long>
    var minPrice = amountsToPrices[0][1]
    var money = 0L
    for (day in 0 until numDays) {
        minPrice = min(minPrice, amountsToPrices[day][1])
        money += amountsToPrices[day][0] * minPrice
    }
    print(money)
}