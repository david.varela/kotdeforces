fun main() {
    val first = readLine()!!
    val second = readLine()!!
    if (first.length != second.length) return print("NO")
    val positions = mutableListOf<Int>()
    for (pos in first.indices) if (first[pos] != second[pos]) positions.add(pos)
    if (positions.size != 2) return print("NO")
    if (first[positions[0]] != second[positions[1]]) return print("NO")
    if (first[positions[1]] != second[positions[0]]) return print("NO")
    print("YES")
}