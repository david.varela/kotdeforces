fun main() {
    readLine()
    val times = mutableMapOf<Char, Int>().withDefault { 0 }
    for (c in readLine()!!) times[c] = times.getValue(c) + 1
    print(
        when (times.getValue('I')) {
            0 -> times.getValue('A')
            1 -> 1
            else -> 0
        }
    )
}