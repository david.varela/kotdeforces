import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (a, b, c, d) = readInts()
    val diff = max(3 * a / 10, a - a / 250 * c) - max(3 * b / 10, b - b / 250 * d)
    print(
        when {
            diff > 0 -> "Misha"
            diff == 0 -> "Tie"
            else -> "Vasya"
        }
    )
}