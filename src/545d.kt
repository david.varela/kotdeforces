fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    var happyPeople = readInt()
    val times = readLongs().sorted()
    var waiting = 0L
    for (person in times) {
        if (waiting <= person) waiting += person else happyPeople--
    }
    print(happyPeople)
}