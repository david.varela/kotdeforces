fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (_, maxSeverity, numTransfers) = readInts()
    val severities = readInts()
    var count = 0
    var sol = 0
    for (severity in severities) {
        if (severity <= maxSeverity) count++
        else {
            if (count >= numTransfers) sol += count + 1 - numTransfers
            count = 0
        }
    }
    if (count >= numTransfers) sol += count + 1 - numTransfers
    print(sol)
}