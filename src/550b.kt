import kotlin.math.pow

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numProblems, minTotalDifficult, maxTotalDifficult, minbiggerDiff) = readInts()
    val difficulties = readInts().sorted()
    var sol = 0
    for (candidate in 1 until 2.0.pow(numProblems).toInt()) {
        val bitMask = candidate.toString(radix = 2)
        var min: Int? = null
        var max: Int? = null
        var sum = 0L
        for (pos in bitMask.indices) {
            val realPos = numProblems - bitMask.length + pos
            if (bitMask[pos] == '1') {
                if (min == null) min = difficulties[realPos]
                max = difficulties[realPos]
                sum += difficulties[realPos]
            }
        }
        if (sum in minTotalDifficult..maxTotalDifficult && max!! - min!! >= minbiggerDiff) sol++
    }
    print(sol)
}