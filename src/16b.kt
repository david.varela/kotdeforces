import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    var (numMatchBoxes, numContainers) = readInts()
    val options = Array(numContainers) { 0 to 0 }
    for (container in 0 until numContainers) {
        val (numBoxes, numMatches) = readInts()
        options[container] = numMatches to numBoxes
    }
    options.sortByDescending { it.first }
    var sol = 0L
    for (option in options) {
        val numBoxes = min(numMatchBoxes, option.second)
        sol += numBoxes * option.first
        numMatchBoxes -= numBoxes
        if (numMatchBoxes == 0) break
    }
    print(sol)
}