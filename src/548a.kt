fun main() {
    val text = readLine()!!
    val k = readLine()!!.toInt()
    if (text.length % k != 0) {
        print("NO")
    } else {
        val pl = text.length / k
        var result = true
        for (i in 0 until k) {
            result = text.isPalindrome(pl * i, pl * (i + 1) - 1)
            if (!result) break
        }
        if (result) print("YES") else print("NO")
    }
}

fun String.isPalindrome(start: Int, end: Int): Boolean {
    for (i in 0 until (end + 1 - start) / 2) {
        if (this[start + i] != this[end - i]) {
            return false
        }
    }
    return true
}