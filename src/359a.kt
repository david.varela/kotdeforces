fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numRows, _) = readInts()
    val dummyList = listOf(0)
    val board = Array(numRows) { dummyList }
    for (row in 0 until numRows) board[row] = readInts()
    if (board.first().contains(1) || board.last().contains(1)) return print(2)
    for (row in 0 until numRows) if (board[row].first() == 1 || board[row].last() == 1) return print(2)
    print(4)
}