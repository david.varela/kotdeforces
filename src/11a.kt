fun main() {
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)
    fun readLongArray(): LongArray {
        val arrText = readLine()!!.split(" ")
        val sol = LongArray(arrText.size)
        for (pos in sol.indices) sol[pos] = arrText[pos].toLong()
        return sol
    }

    val (n, d) = readLongs()
    val arr = readLongArray()
    var sol = 0L
    for (pos in 1 until n.toInt()) {
        val diff = -(arr[pos] - arr[pos - 1] - 1)
        if (diff > 0) {
            val necessary = diff / d + if (diff % d == 0L) 0 else 1
            sol += necessary
            arr[pos] += necessary * d
        }
    }
    print(sol)
}