import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (a, b, c) = readInts()
    val (x, y, z) = readInts()
    var necessary = max(0, x - a) + max(0, y - b) + max(0, z - c)
    necessary -= max(0, a - x) / 2 + max(0, b - y) / 2 + max(0, c - z) / 2
    print(if (necessary <= 0) "Yes" else "No")
}