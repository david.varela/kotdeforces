fun main() {
    val (a, b) = readLine()!!.split(" ")
    print(if (a == b) a else 1)
}