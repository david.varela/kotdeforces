fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numCards = readInt()
    val cards = readInts()
    var left = 0
    var right = numCards - 1
    val points = intArrayOf(0, 0)
    var turn = 0
    while (left <= right) {
        if (cards[left] >= cards[right]) {
            points[turn] += cards[left]
            left++
        } else {
            points[turn] += cards[right]
            right--
        }
        turn = turn xor 1
    }
    print(points.joinToString(" "))
}