fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val n = readInt()
    val times = IntArray(1001)
    for (num in readInts()) times[num]++
    val limit = (n + 1) / 2
    for (time in times) if (time > limit) return print("NO")
    print("YES")
}