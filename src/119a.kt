fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readLong() = readLine()!!.toLong()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    infix fun Long.maxMin(b: Long): Pair<Long, Long> {
        return if (this >= b) this to b else b to this
    }

    fun gcd(a: Long, b: Long): Long {
        var (max, min) = a maxMin b
        while (min != 0L) {
            val newMin = max % min
            max = min
            min = newMin
        }
        return max
    }

    var (a, b, n) = readLongs()

    while (true) {
        n -= gcd(a, n)
        if (n < 0) {
            print(1)
            return
        }
        n -= gcd(b, n)
        if (n < 0) {
            print(0)
            return
        }
    }
}