import kotlin.math.abs

fun main() {
    fun readInt() = readLine()!!.toInt()

    val numTrees = readInt()
    val heights = IntArray(numTrees)
    for (pos in 0 until numTrees) heights[pos] = readInt()
    var sol = numTrees + (numTrees - 1) + heights[0]
    for (pos in 1 until numTrees) sol += abs(heights[pos] - heights[pos - 1])
    print(sol)
}