fun main() {
    fun readInt() = readLine()!!.toInt()

    val numChanges = readInt()
    val newToOld = mutableMapOf<String, String>()
    repeat(numChanges) {
        val (old, new) = readLine()!!.split(" ")
        if (old in newToOld) {
            newToOld[new] = newToOld[old]!!
            newToOld.remove(old)
        } else newToOld[new] = old
    }
    println(newToOld.size)
    print(newToOld.entries.joinToString(System.lineSeparator()) { "${it.value} ${it.key}" })
}