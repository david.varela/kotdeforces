fun main() {
    val text = readLine()!!
    print(
        when {
            text == "30 of month" ->  11
            text == "31 of month" -> 7
            text.endsWith("month") -> 12
            text[0] == '5' -> 53
            text[0] == '6' -> 53
            else -> 52
        }
    )
}