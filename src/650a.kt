fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numWatchmen = readInt()
    val occsX = mutableMapOf<Int, Int>()
    val occsY = mutableMapOf<Int, Int>()
    val occsXY = mutableMapOf<Pair<Int, Int>, Int>()
    var result = 0L
    repeat(numWatchmen) {
        val (x, y) = readInts()
        result += occsX.getOrDefault(x, 0) + occsY.getOrDefault(y, 0) - occsXY.getOrDefault(x to y, 0)
        occsX[x] = occsX.getOrDefault(x, 0) + 1
        occsY[y] = occsY.getOrDefault(y, 0) + 1
        occsXY[x to y] = occsXY.getOrDefault(x to y, 0) + 1
    }
    print(result)
}
