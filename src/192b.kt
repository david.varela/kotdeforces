import kotlin.math.max
import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numTiles = readInt()
    val numDays = readInts()
    var sol = min(numDays.first(), numDays.last())
    for (pos in 1 until numTiles) sol = min(sol, max(numDays[pos], numDays[pos - 1]))
    print(sol)
}