fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    val years = readInts()
    val (a, b) = readInts()
    print(years.slice(a - 1 until b - 1).sum())
}