fun main() {
    val sides = readLine()!!.split(" ").map(String::toInt).sorted()
    var output = "IMPOSSIBLE"
    for (pos in 1..2) {
        if (sides[pos - 1] + sides[pos] > sides[pos + 1]) {
            output = "TRIANGLE"
            break
        } else if (sides[pos - 1] + sides[pos] == sides[pos + 1]) {
            output = "SEGMENT"
        }
    }
    print(output)
}