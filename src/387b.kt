fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numNecessaryProblems, numAvailableProblems) = readInts()
    val requirements = readInts()
    val available = readInts()
    var reqPos = 0
    var availablePos = 0
    while (reqPos < numNecessaryProblems && availablePos < numAvailableProblems) {
        if (requirements[reqPos] <= available[availablePos])
            reqPos++
        availablePos++
    }
    print(numNecessaryProblems - reqPos)
}