import kotlin.math.ceil
import kotlin.math.sqrt

fun main() {
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    val (r, x, y, x2, y2) = readLongs()
    val d = sqrt(((x - x2) * (x - x2) + (y - y2) * (y - y2)).toDouble())
    print(ceil(d / (r * 2)).toLong())
}