fun main() {
    fun readInt() = readLine()!!.toInt()

    val bitsPerCell = readInt()
    val s = readLine()!!
    var sol = 0
    for (c in s)
        if (c == '1') sol++ else return print(sol + 1)
    print(bitsPerCell)
}