import kotlin.math.max

fun main() {
    val numRecords = readLine()!!.toInt()
    var sol = 0
    val regNumbers = mutableSetOf<String>()
    for (i in 1..numRecords) {
        val (action, regNumber) = readLine()!!.split(" ")
        if (action == "+")
            regNumbers.add(regNumber)
        else {
            if (regNumber in regNumbers) {
                sol = max(sol, regNumbers.size)
                regNumbers.remove(regNumber)
            } else {
                sol = max(sol + 1, regNumbers.size + 1)
            }
        }
    }
    sol = max(sol, regNumbers.size)
    print(sol)
}