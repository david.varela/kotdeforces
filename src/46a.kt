fun main() {
    fun readInt() = readLine()!!.toInt()

    val n = readInt()
    var pos = 0
    val sols = IntArray(n - 1)
    for (pass in 1 until n) {
        pos = (pos + pass) % n
        sols[pass - 1] = pos + 1
    }
    print(sols.joinToString(" "))
}