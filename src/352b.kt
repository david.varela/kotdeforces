import java.util.*

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readInt()
    val arr = readInts()
    val discarded = mutableSetOf<Int>()
    val last = mutableMapOf<Int, Int>()
    val delta = TreeMap<Int, Int>().withDefault { Int.MIN_VALUE }
    for ((pos, a) in arr.withIndex())
        if (a !in discarded)
            when (delta.getValue(a)) {
                Int.MIN_VALUE -> {
                    last[a] = pos
                    delta[a] = 0
                }
                0 -> delta[a] = pos - last[a]!!
                else -> {
                    if (arr[pos - delta[a]!!] != a) {
                        discarded.add(a)
                        last.remove(a)
                        delta.remove(a)
                    }
                }
            }
    println(delta.size)
    print(delta.entries.joinToString(separator = System.lineSeparator()) { "${it.key} ${it.value}" })
}