fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    val (length, sum) = readInts()
    var quotient = sum / 9
    var rest = sum % 9
    if (length == 1 && sum == 0) {
        print("0 0")
        return
    }
    if (length == 0 || sum == 0 || quotient > length || (quotient == length && rest != 0)) {
        print("-1 -1")
        return
    }
    val sb1 = StringBuilder()
    for (i in 0 until quotient) sb1.append('9')
    if (quotient < length) sb1.append(rest)
    for (i in 0 until length - quotient - 1)
        sb1.append('0')
    val sb2 = StringBuilder()
    var oneEight = false
    if (length > quotient + 1) {
        rest--
        if (rest < 0) {
            rest = 0
            oneEight = true
        }
        sb2.append('1')
        for (i in 0 until length - quotient - 2) sb2.append('0')
    }
    if (quotient < length) sb2.append(rest)
    if(oneEight) {
        sb2.append('8')
        quotient--
    }
    for (i in 0 until quotient) sb2.append('9')
    print("$sb2 $sb1")
}