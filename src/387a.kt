fun main() {
    val (hourWokeUp, minutesWokeUp) = readLine()!!.split(":").map(String::toInt)
    val wokeUp = 60 * hourWokeUp + minutesWokeUp
    val (hourSleep, minutesSleep) = readLine()!!.split(":").map(String::toInt)
    val sleep = 60 * hourSleep + minutesSleep
    val sol = (wokeUp + 24 * 60 - sleep) % (24 * 60)
    print("${String.format("%02d", sol / 60)}:${String.format("%02d", sol % 60)}")
}