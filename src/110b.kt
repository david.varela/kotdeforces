fun main() {
    fun readInt() = readLine()!!.toInt()

    val length = readInt()
    val sb = StringBuilder()
    for (pos in 0 until length) sb.append('a' + pos % 4)
    print(sb.toString())
}