fun main() {
    fun readInt() = readLine()!!.toInt()

    val numHalfDigits = readInt()
    val ticket = readLine()!!
    val firstHalf = ticket.substring(0 until numHalfDigits).map { c -> c.toInt() }.sorted()
    val secondHalf = ticket.substring(numHalfDigits).map { c -> c.toInt() }.sorted()
    if (firstHalf[0] == secondHalf[0]) return print("NO")
    val (bigger, smaller) = if (firstHalf[0] > secondHalf[0]) firstHalf to secondHalf else secondHalf to firstHalf
    for (pos in bigger.indices)
        if (bigger[pos] <= smaller[pos]) return print("NO")
    print("YES")
}