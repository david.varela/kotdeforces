fun main() {
    val n = readLine()!!.toInt()
    val beauties = readLine()!!.split(" ").map { it.toInt() }
    val groups = mutableMapOf<Int, Int>().withDefault { 0 }
    beauties.forEach {
        groups[it] = groups.getValue(it).inc()
    }
    val extraGroups = groups.values.maxOrNull()!! - 1
    print(n - extraGroups - 1)
}