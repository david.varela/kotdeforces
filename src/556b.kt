fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numGears = readInt()
    val active = readInts()
    val diff = numGears - active[0]
    for (pos in 0 until numGears step 2)
        if ((active[pos] + diff) % numGears != pos) return print("No")
    for (pos in 1 until numGears step 2)
        if ((active[pos] + numGears - diff) % numGears != pos) return print("No")
    print("Yes")
}