import kotlin.math.max
import kotlin.math.min

fun main() {
    val (a, b) = readLine()!!.split(" ").map { it.toInt() }
    val d  = min(a, b)
    val s = max((a - d) / 2, (b - d) / 2)
    print("$d $s")
}