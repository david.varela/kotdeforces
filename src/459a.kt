import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

fun main() {
    val (x1, y1, x2, y2) = readLine()!!.split(" ").map { it.toInt() }
    when {
        x1 == x2 && y1 != y2 -> print("${x1 + max(y1, y2) - min(y1, y2)} $y1 ${x2 + max(y1, y2) - min(y1, y2)} $y2")
        x1 != x2 && y1 == y2 -> print("$x1 ${y1 + max(x1, x2) - min(x1, x2)} $x2 ${y2 + max(x1, x2) - min(x1, x2)}")
        abs(x1 - x2) == abs(y1 - y2) -> print("$x1 $y2 $x2 $y1")
        else -> print(-1)
    }
}