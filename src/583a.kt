fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numRoads = readInt()
    val sol = mutableListOf<Int>()
    val visitedRows = mutableSetOf<Int>()
    val visitedColumns = mutableSetOf<Int>()
    for (day in 1..numRoads * numRoads) {
        val (row, column) = readInts()
        if (row !in visitedRows && column !in visitedColumns) {
            sol.add(day)
            visitedRows.add(row)
            visitedColumns.add(column)
        }
    }
    print(sol.joinToString(" "))
}