fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val n = readInt()
    val a = readInts()
    val b = a.sorted()
    var count = 0
    for (pos in 0 until n) {
        if (a[pos] != b[pos]) count++
    }
    print(if (count <= 2) "YES" else "NO")
}