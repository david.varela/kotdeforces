fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numApples = readInt()
    val numSmall = readInts().count { it == 100 }
    print(
        when {
            numSmall == 0 && numApples and 1 == 1 -> "NO"
            numSmall and 1 == 1 -> "NO"
            else -> "YES"
        }
    )

}