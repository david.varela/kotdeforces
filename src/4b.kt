import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numDays, sumTime) = readInts()
    val mins = IntArray(numDays)
    val maxs = IntArray(numDays)
    for (i in 0 until numDays) {
        val (min, max) = readInts()
        mins[i] = min
        maxs[i] = max
    }
    val sumMins = mins.sum()
    if (sumTime in sumMins..maxs.sum()) {
        println("YES")
        var diff = sumTime - sumMins
        val sols = IntArray(numDays)
        for (day in 0 until numDays) {
            val extras = min(diff, maxs[day] - mins[day])
            sols[day] = mins[day] + extras
            diff -= extras
        }
        print(sols.joinToString(" "))
    } else print("NO")
}