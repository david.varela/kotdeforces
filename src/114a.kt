fun main() {
    fun readInt(): Int = readLine()!!.toInt()

    val k = readInt()
    var l = readInt()
    var count = -1
    while (l > 1)
        if (l % k != 0)
            return print("NO")
        else {
            l /= k
            count++
        }
    println("YES")
    print(count)
}
