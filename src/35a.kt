import java.io.File

fun main() {
    val lines = File("input.txt").readLines()
    var position = lines[0].toInt()
    for (line in 1..3) {
        val change = lines[line].split(" ").map(String::toInt).toMutableSet()
        if (position in change) {
            change.remove(position)
            position = change.first()
        }
    }
    File("output.txt").writeText(position.toString())
}