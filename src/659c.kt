fun main() {
    var (_, money) = readLine()!!.split(" ").map { it.toInt() }
    val has = readLine()!!.split(" ").map { it.toInt() }.toSet()
    var numBuys = 0
    var candidate = 1
    val buys = mutableSetOf<Int>()
    while (money - candidate >= 0 && candidate > 0) {
        if (candidate !in has) {
            numBuys++
            money -= candidate
            buys.add(candidate)
        }
        candidate++
    }
    val sb = StringBuilder()
    sb.appendLine(numBuys)
    sb.append(buys.joinToString(" "))
    print(sb.toString())
}