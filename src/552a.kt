fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    var sol = 0
    repeat(readInt()) {
        val (x1, y1, x2, y2) = readInts()
        sol += (x2 - x1 + 1) * (y2 - y1 + 1)
    }
    print(sol)
}