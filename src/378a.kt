import kotlin.math.abs

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (a, b) = readInts()
    var first = 0
    var second = 0
    for (result in 1..6) {
        when {
            abs(a - result) < abs(b - result) -> first++
            abs(a - result) > abs(b - result) -> second++
        }
    }
    print("$first ${6 - first - second} $second")
}