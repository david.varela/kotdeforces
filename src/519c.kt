import kotlin.math.max
import kotlin.math.min

fun main() {
    val (n, m) = readLine()!!.split(" ").map { it.toInt() }
    val big = max(m, n)
    val small = min(m, n)
    val diff = big - small
    if (diff > small) {
        print(small)
    } else {
        print((diff + (small - diff) * 2 / 3).toLong())
    }
}