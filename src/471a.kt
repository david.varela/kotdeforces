fun main() {
    val s = readLine()!!.split(" ").map { it.toInt() }
    val ms = mutableMapOf<Int, Int>()
    s.forEach { ms.merge(it, 1, Int::plus) }
    val l = ms.values.sorted()
    when {
        l.size == 1 -> print("Elephant")
        l.size == 2 && l[1] == 4 -> print("Elephant")
        l.size == 2 && l[1] == 5 -> print("Bear")
        l.size == 3 && l[2] == 4 && l[1] == 1 -> print("Bear")
        else -> print("Alien")
    }
}
