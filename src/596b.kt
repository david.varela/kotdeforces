import kotlin.math.abs

fun main() {
    readLine()
    val a = readLine()!!.split(" ").map { it.toLong() }
    var changes = 0L
    var last = 0L
    a.forEach {
        changes += abs(last - it)
        last = it
    }
    print(changes)
}