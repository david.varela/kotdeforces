fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (_, maxLuckyDigits) = readInts()
    val lucky = listOf('4', '7')
    val nums = readLine()!!.split(" ")
    print(nums.map { it.count { c -> c in lucky } }.count { it <= maxLuckyDigits })
}