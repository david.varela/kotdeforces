fun main() {
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    readLine()
    val arr = readLongs()
    val numOdds = arr.count { it % 2 == 1L }
    val minOdd = arr.reduce { acc, i -> if (acc % 2 == 0L) i else if (i % 2 == 1L && i < acc) i else acc }
    print(arr.sum() - if (numOdds % 2 == 0) 0 else minOdd)
}