fun main() {
    val (numCandies, numFriends) = readLine()!!.split(" ").map(String::toInt)
    val sol = IntArray(numFriends)
    val less = numCandies / numFriends
    val more = less + if (numCandies % numFriends == 0) 0 else 1
    val numLess = numFriends - numCandies % numFriends
    for (pos in 0 until numLess) sol[pos] = less
    for (pos in numLess until numFriends) sol[pos] = more
    print(sol.joinToString(" "))
}