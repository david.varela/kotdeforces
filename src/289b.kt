import java.util.*

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (n, _, d) = readInts()
    val elements = TreeMap<Int, Int>()
    for (r in 0 until n) {
        val aes = readInts()
        for (a in aes) {
            elements[a] = elements.getOrDefault(a, 0) + 1
        }
    }

    var sol = 0L
    while (elements.size > 1) {
        if (elements.firstEntry().key + d > elements.lastEntry().key) {
            print(-1)
            return
        }
        if (elements.firstEntry().value <= elements.lastEntry().value) {
            sol += elements.firstEntry().value
            elements[elements.firstKey() + d] =
                elements.getOrDefault(elements.firstKey() + d, 0) + elements.firstEntry().value
            elements.remove(elements.firstKey())
        } else {
            sol += elements.lastEntry().value
            elements[elements.lastKey() - d] =
                elements.getOrDefault(elements.lastKey() - d, 0) + elements.lastEntry().value
            elements.remove(elements.lastKey())
        }
    }
    print(sol)
}
