fun main() {
    val numPeople = readLine()!!.toInt()
    val names = Array(numPeople) { listOf("", "") }
    for (pos in 0 until numPeople) names[pos] = readLine()!!.split(" ")
    val positions = readLine()!!.split(" ").map { it.toInt() - 1 }
    var prev = names[positions[0]].minOrNull()!!
    for (position in positions) {
        prev = when {
            names[position].minOrNull()!! >= prev -> names[position].minOrNull()!!
            names[position].maxOrNull()!! >= prev -> names[position].maxOrNull()!!
            else -> return print("NO")
        }
    }
    print("YES")
}