fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numBooks, numGenres) = readInts()
    val times = LongArray(numGenres + 1)
    for (genre in readInts()) times[genre]++
    print(times.fold(0L) { acc, genreTimes -> acc + genreTimes * (numBooks - genreTimes) } / 2)
}