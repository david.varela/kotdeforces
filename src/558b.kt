fun main() {
    readLine()
    val arr = readLine()!!.split(" ").map(String::toInt)
    val occs = mutableMapOf<Int, Int>().withDefault { 0 }
    val first = mutableMapOf<Int, Int>()
    val last = mutableMapOf<Int, Int>()
    for ((pos, element) in arr.withIndex()) {
        occs[element] = occs.getValue(element) + 1
        if (element !in first) first[element] = pos
        last[element] = pos
    }
    val biggerFrequency = occs.entries.maxByOrNull { it.value }!!.value
    val frequents = occs.entries.filter { it.value == biggerFrequency }
    var bestFrequent = frequents.first()
    var bestFrequency = last[bestFrequent.key]!! - first[bestFrequent.key]!!
    for (frequent in frequents) {
        val frequency = last[frequent.key]!! - first[frequent.key]!!
        if (frequency < bestFrequency) {
            bestFrequency = frequency
            bestFrequent = frequent
        }
    }
    print("${first[bestFrequent.key]!! + 1} ${last[bestFrequent.key]!! + 1}")
}