import kotlin.math.min

fun main() {
    var (_, numImprovements) = readLine()!!.split(" ").map(String::toInt)
    val skills = readLine()!!.split(" ").map(String::toInt).sortedByDescending { it % 10 }.toIntArray()
    for ((pos, skill) in skills.withIndex()) {
        if (skill == 100) continue
        val delta = 10 - skill % 10
        if (delta > numImprovements) break
        skills[pos] += delta
        numImprovements -= delta
        if (delta == 0) break
    }
    if (numImprovements >= 10) {
        // all the skills are in the tenth
        for ((pos, skill) in skills.withIndex()) {
            if (skill == 100) continue
            if (numImprovements < 10) break
            val delta = min(numImprovements, 100 - skill)
            skills[pos] += delta
            numImprovements -= delta
        }
    }
    print(skills.map { it / 10 }.sum())
}