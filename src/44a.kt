fun main() {
    fun readInt() = readLine()!!.toInt()

    val leaves = mutableSetOf<String>()
    for (i in 1..readInt()) leaves.add(readLine()!!)
    print(leaves.size)
}