fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    repeat(readInt()) {
        var (a, b) = readInts()
        var sol = 0
        while (a != 0 && b != 0) {
            if (a < b) {
                sol += b / a
                b %= a
            } else {
                sol += a / b
                a %= b
            }
        }
        println(sol)
    }
}