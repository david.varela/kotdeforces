import kotlin.math.min
import kotlin.math.sqrt

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (n, x) = readInts()
    val end = min(n.toDouble(), sqrt(x.toDouble())).toInt()
    var sol = 0
    for (i in 1..end)
        if (x % i == 0 && x / i <= n)
            sol += if (x / i == i) 1 else 2
    print(sol)
}