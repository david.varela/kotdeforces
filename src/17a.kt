fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    fun sieve(limit: Int): List<Int> {
        val notPrimes = mutableSetOf<Int>()
        val primes = ArrayList<Int>()
        for (num in 2..limit) {
            if (num !in notPrimes) {
                primes.add(num)
                var notPrime = num + num
                while (notPrime <= limit) {
                    notPrimes.add(notPrime)
                    notPrime += num
                }
            }
        }
        return primes
    }
    val (n, k) = readInts()
    val primes = sieve(n)
    var sol = 0
    for (goal in primes)
        for (pos in 1 until primes.size)
            if (primes[pos] + primes[pos - 1] + 1 > goal) break
            else if (primes[pos] + primes[pos - 1] + 1 == goal) {
                sol++
                break
            }
    print(if (sol >= k) "YES" else "NO")
}