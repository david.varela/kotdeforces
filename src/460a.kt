fun main() {
    var (n, m) = readLine()!!.split(" ").map { it.toInt() }
    m--
    val quotient = n / m
    val rest = n % m
    m++
    if (rest == 0) print(quotient * m - 1) else print(quotient * m + rest)
}