fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numEmployees, _) = readInts()
    val canTalk = mutableSetOf<MutableSet<Int>>()
    val seenLanguages = mutableSetOf<Int>()
    var noLanguages = 0
    repeat(numEmployees) {
        val input = readInts()
        if (input[0] == 0) noLanguages++
        else {
            val languages = input.slice(1..input[0]).toMutableSet()
            if (languages.intersect(seenLanguages).isEmpty())
                canTalk.add(languages)
            else {
                val toRemove = mutableSetOf<MutableSet<Int>>()
                var selected: MutableSet<Int>? = null
                for (group in canTalk)
                    if (group.intersect(languages).isNotEmpty())
                        if (selected == null) {
                            selected = group
                            group.addAll(languages)
                        } else {
                            selected.addAll(group)
                            toRemove.add(group)
                        }
                canTalk.removeAll(toRemove)
            }
            seenLanguages.addAll(languages)
        }
    }
    print(noLanguages + if (canTalk.isNotEmpty()) canTalk.size - 1 else 0)
}