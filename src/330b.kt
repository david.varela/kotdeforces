fun main() {
    val (numCities, numInvalidRoads) = readLine()!!.split(" ").map(String::toInt)
    val cities = mutableSetOf(*(1..numCities).toList().toTypedArray())
    for (i in 1..numInvalidRoads) {
        cities.removeAll(readLine()!!.split(" ").map(String::toInt))
    }
    println(numCities - 1)
    val selected = cities.first()
    val sb = StringBuilder()
    for (i in 1..numCities) {
        if (i != selected) {
            sb.appendLine("$selected $i")
        }
    }
    print(sb.toString())
}