import kotlin.math.abs
import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (x1, y1) = readInts()
    val (x2, y2) = readInts()
    print(max(abs(x1 - x2), abs(y1 - y2)))
}