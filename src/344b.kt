fun main() {
    val (a, b, c) = readLine()!!.split(" ").map(String::toInt)
    if ((a + b + c) % 2 == 1) return print("Impossible")
    val y = (b - a + c) / 2
    val x = a + y - c
    val z = a - x
    if (x < 0 || y < 0 || z < 0) return print("Impossible")
    print("$x $y $z")
}