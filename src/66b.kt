import kotlin.math.max

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numSections = readInt()
    val sections = readInts()
    val smallerAtLeft = IntArray(numSections)
    for (pos in 1 until numSections)
        if (sections[pos - 1] <= sections[pos])
            smallerAtLeft[pos] = smallerAtLeft[pos - 1] + 1
    var sol = smallerAtLeft.last()
    var smallerRight = 0
    for (pos in numSections - 2 downTo 0) {
        if (sections[pos] >= sections[pos + 1]) smallerRight++ else smallerRight = 0
        sol = max(sol, smallerAtLeft[pos] + smallerRight)
    }
    print(sol + 1)
}