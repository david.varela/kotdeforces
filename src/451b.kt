fun main() {
    val n = readLine()!!.toInt()
    val a = readLine()!!.split(" ").map(String::toInt)
    var state = 1
    var start = Integer.MIN_VALUE
    var end = Integer.MAX_VALUE
    loop@ for (pos in 1 until n) {
        if (a[pos] < a[pos - 1]) {
            when (state) {
                1 -> {
                    start = pos - 1
                    state++
                }
                3 -> {
                    state = -1
                    break@loop
                }
            }
        } else {
            if (state == 2) {
                end = pos - 1
                state++
            }
        }
    }
    if (end == Int.MAX_VALUE)
        end = n - 1
    if (state == -1 || (start > 0 && a[start - 1] > a[end]) || (end < n - 1 && a[end + 1] < a[start]))
        print("no")
    else {
        println("yes")
        if (state == 1) print("1 1") else print("${start + 1} ${end + 1}")
    }
}