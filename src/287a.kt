fun main() {
    fun Boolean.toInt() = if (this) 1 else 0
    val board = Array(4) { readLine()!! }
    for (r in 0..2)
        for (c in 0..2) {
            var counter = 0
            counter += (board[r][c] == board[r][c + 1]).toInt()
            counter += (board[r][c] == board[r + 1][c]).toInt()
            counter += (board[r][c] == board[r + 1][c + 1]).toInt()
            if (counter != 1) {
                print("YES")
                return
            }
        }
    print("NO")
}