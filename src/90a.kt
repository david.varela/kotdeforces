import kotlin.math.max

fun main() {
    val colors = readLine()!!.split(" ").map(String::toInt)
    var sol = 0
    for ((pos, numPeople) in colors.withIndex()) {
        if (numPeople > 0) {
            val numCars = numPeople / 2 + if (numPeople % 2 != 0) 1 else 0
            sol = max(sol, pos + 3 * (numCars - 1) + 30)
        }
    }
    print(sol)
}