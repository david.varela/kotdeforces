fun main() {
    val a = readLine()!!
    val index = a.indexOf('0')
    if (index == -1) print(a.substring(1)) else {
        print(a.substring(0, index))
        print(a.substring(index + 1))
    }
}