fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val posToPos = mutableMapOf<Int, Int>()
    repeat(readLine()!!.toInt()) {
        val (position, spit) = readInts()
        posToPos[position] = position + spit
    }
    for (entry in posToPos)
        if (entry.value in posToPos && posToPos[entry.value] == entry.key) return print("YES")
    print("NO")
}
