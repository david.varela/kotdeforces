fun main() {
    fun readInt() = readLine()!!.toInt()
    fun sieve(limit: Int): List<Int> {
        val notPrimes = mutableSetOf<Int>()
        val primes = ArrayList<Int>()
        for (num in 2..limit) {
            if (num !in notPrimes) {
                primes.add(num)
                var notPrime = num + num
                while (notPrime <= limit) {
                    notPrimes.add(notPrime)
                    notPrime += num
                }
            }
        }
        return primes
    }

    val n = readInt()
    val primes = sieve(n)
    var result = primes.size
    val squares = mutableListOf<Int>()
    for (p in primes) {
        var other = p * p
        while (other <= n) {
            result++
            squares.add(other)
            other *= p
        }
    }
    println(result)
    squares.addAll(primes)
    print(squares.joinToString(" "))
}