fun main() {
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    readLine()
    val times = readLongs()
    val used = mutableSetOf<Long>()
    var sol = 0L
    for (option in times)
        for (i in option downTo 0)
            if (i !in used) {
                used.add(i)
                sol += i
                break
            }
    print(sol)
}