fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val length = readInt()
    val strip = readLine()!!
    val jumps = readInts()
    val visited = mutableSetOf<Int>()
    var pos = 0
    while(true) {
        if (pos in visited) return print("INFINITE")
        if (pos < 0 || pos >= length) return print("FINITE")
        visited.add(pos)
        pos = if(strip[pos] == '<') pos - jumps[pos] else pos + jumps[pos]
    }
}