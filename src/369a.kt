fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    var (_, numBowls, numPlates) = readInts()
    val days = readInts()
    var sol = 0
    for (day in days)
        when {
            day == 1 && numBowls > 0 -> numBowls--
            day == 1 -> sol++
            day == 2 && numPlates > 0 -> numPlates--
            day == 2 && numBowls > 0 -> numBowls--
            else -> sol++
        }
    print(sol)
}