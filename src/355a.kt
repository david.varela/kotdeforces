fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numDigits, sumResult) = readInts()
    if (sumResult == 0 && numDigits > 1) return print("No solution")
    val sb = StringBuilder()
    sb.append(sumResult)
    for (i in 1 until numDigits) sb.append('9')
    print(sb.toString())
}