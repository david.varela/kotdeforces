fun main() {
    fun sieve(limit: Long): List<Long> {
        val notPrimes = mutableSetOf<Long>()
        val primes = ArrayList<Long>()
        for (num in 2..limit) {
            if (num !in notPrimes) {
                primes.add(num)
                var notPrime = num + num
                while (notPrime <= limit) {
                    notPrimes.add(notPrime)
                    notPrime += num
                }
            }
        }
        return primes
    }

    val primes = sieve(1000000)
    val tPrimes = primes.map { it * it }.toSet()
    readLine()
    val sb = StringBuilder()
    for (number in readLine()!!.split(" ").map(String::toLong)) {
        if (number in tPrimes) sb.appendLine("YES") else sb.appendLine("NO")
    }
    print(sb.toString())
}