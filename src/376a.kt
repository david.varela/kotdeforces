fun main() {
    val bar = readLine()!!
    val middle = bar.indexOf('^')
    var left = 0L
    for (pos in 0 until middle) if (bar[pos] != '=') left += (middle - pos) * (bar[pos] - '0')
    var right = 0L
    for (pos in middle + 1 until bar.length) if (bar[pos] != '=') right += (pos - middle) * (bar[pos] - '0')
    print(
        when {
            left == right -> "balance"
            left > right -> "left"
            else -> "right"
        }
    )
}