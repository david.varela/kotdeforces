fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (watchPerSecond, downloadPerSecond, seconds) = readInts()
    var wait = (seconds * watchPerSecond - seconds * downloadPerSecond) / downloadPerSecond
    if (wait * downloadPerSecond + seconds * downloadPerSecond < watchPerSecond * seconds) wait++
    print(wait)
}