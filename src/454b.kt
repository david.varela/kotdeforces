fun main() {
    val length = readLine()!!.toInt()
    val a = readLine()!!.split(" ").map(String::toInt)
    var changes = 0
    for (pos in 1 until length) {
        if (a[pos] < a[pos - 1]) {
            if (changes == 0 && a.last() <= a.first()) {
                changes = length - pos
            } else {
                changes = -1
                break
            }
        }
    }
    print(changes)
}