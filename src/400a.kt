fun main() {
    val lengths = listOf(1, 2, 3, 4, 6, 12)
    repeat(readLine()!!.toInt()) {
        val cards = readLine()!!
        val sols = mutableListOf<Int>()
        nextLength@for (length in lengths) {
            nextStart@for (start in 0 until length) {
                for (pos in start until 12 step  length)
                    if (cards[pos] != 'X') continue@nextStart
                sols.add(length)
                continue@nextLength
            }
        }
        println("${sols.size} ${sols.asReversed().joinToString(" ") { "${12/it}x${it}" }}")
    }
}