fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (n, m) = readInts()
    var sol = 0
    var a = -1
    while (++a * a <= n) {
        val b = n - a * a
        if (a + b * b == m) sol++
    }
    print(sol)
}