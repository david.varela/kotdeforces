import java.util.*

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numExams = readInt()
    val exams = TreeSet<Pair<Int, Int>>(compareBy({ it.first }, { it.second }))
    repeat(numExams) {
        val (realDay, advancedDay) = readInts()
        exams.add(advancedDay to realDay)
    }
    while (exams.size > 1) {
        val previous = exams.first()
        exams.remove(previous)
        if (exams.first().second < previous.second) exams.add(previous.second to previous.second)
    }
    print(exams.first().first)
}