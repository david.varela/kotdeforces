fun main() {
    val maxNumber = readLine()!!.toLong()
    val numMonths = readLine()!!.toInt()
    print(readLine()!!.split(" ").map{ maxNumber - it.toLong() }.subList(0, numMonths - 1).sum())
}