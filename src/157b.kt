import kotlin.math.PI

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numCircles = readInt()
    val radius = readInts().sortedDescending()
    var sol = 0.0
    for (pos in 0 until numCircles step 2) {
        sol += if (pos == radius.lastIndex)
            PI * radius[pos] * radius[pos]
        else
            PI * radius[pos] * radius[pos] - PI * radius[pos + 1] * radius[pos + 1]
    }
    print(sol)
}