fun main() {
    val text = readLine()!!
    if (text.length == 1) return print(0)
    var sol = 1
    var sum = text.fold(0) { acc, i -> acc + (i - '0') }
    while (sum > 9) {
        sol++
        var newSum = 0
        while (sum > 0) {
            newSum += sum % 10
            sum /= 10
        }
        sum = newSum
    }
    print(sol)
}