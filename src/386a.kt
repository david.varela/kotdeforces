fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    var maxPrice = Int.MIN_VALUE
    var maxMaxPRice = Int.MIN_VALUE
    var winner = -1
    for ((pos, price) in readInts().withIndex()) {
        if (price > maxMaxPRice) {
            maxPrice = maxMaxPRice
            maxMaxPRice = price
            winner = pos + 1
        } else if (price > maxPrice) maxPrice = price
    }
    print("$winner $maxPrice")
}