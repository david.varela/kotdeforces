fun main() {
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    infix fun Long.maxMin(b: Long): Pair<Long, Long> {
        return if (this >= b) this to b else b to this
    }

    fun gcd(a: Long, b: Long): Long {
        var (max, min) = a maxMin b
        while (min != 0L) {
            val newMin = max % min
            max = min
            min = newMin
        }
        return max
    }

    fun mcm(a: Long, b: Long): Long {
        return a * b / gcd(a, b)
    }
    val (x, y, a, b) = readLongs()
    val m = mcm(x, y)
    val start = a / m + if (a % m == 0L) 0 else 1
    val end = b / m
    print(end - start + 1)
}