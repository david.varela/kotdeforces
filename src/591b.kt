fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val originToEnd = mutableMapOf<Char, Char>().withDefault { it }
    val middleToOrigin = mutableMapOf<Char, Char>().withDefault { it }
    val (_, numDesigners) = readInts()
    val word = readLine()!!
    repeat(numDesigners) {
        val (c1, c2) = readLine()!!.split(" ").map(String::first)
        val middleToOriginC2 = middleToOrigin.getValue(c1)
        originToEnd[middleToOrigin.getValue(c1)] = c2
        val middleToOriginC1 = middleToOrigin.getValue(c2)
        originToEnd[middleToOrigin.getValue(c2)] = c1
        middleToOrigin[c1] = middleToOriginC1
        middleToOrigin[c2] = middleToOriginC2
    }
    val sb = StringBuilder()
    for (c in word) sb.append(originToEnd.getValue(c))
    print(sb)
}