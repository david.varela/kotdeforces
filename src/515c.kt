fun main() {
    val map = mutableMapOf<Char, List<Int>>()
    map['0'] = listOf()
    map['1'] = listOf()
    map['2'] = listOf(2)
    map['3'] = listOf(3)
    map['4'] = listOf(2, 2, 3)
    map['5'] = listOf(5)
    map['6'] = listOf(3, 5)
    map['7'] = listOf(7)
    map['8'] = listOf(2, 2, 2, 7)
    map['9'] = listOf(3, 3, 2, 7)
    val sol = mutableListOf<Int>()

    readLine()
    val s = readLine()!!
    for (c in s) sol.addAll(map[c]!!)
    sol.sortDescending()
    print(sol.joinToString(""))
}