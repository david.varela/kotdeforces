import kotlin.math.max
import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val info = readInts()
    val smaller = info.subList(0, 4).minOrNull()!!
    val top = min(smaller, info[5] + 1)
    print(max(0, top - info[4]))
}