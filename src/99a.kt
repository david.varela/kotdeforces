fun main() {
    val num = readLine()!!
    val dotPos = num.indexOf('.')
    if (num[dotPos - 1] == '9') return print("GOTO Vasilisa.")
    print(num.substring(0, dotPos - 1) + if (num[dotPos + 1] < '5') num[dotPos - 1] else num[dotPos - 1] + 1)
}