fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val dimension = readInt()
    val board = Array(dimension) { IntArray(dimension) }
    for (row in 0 until dimension) board[row] = readInts().toIntArray()
    val rowsSums = IntArray(dimension)
    val columnsSums = IntArray(dimension)
    for (row in 0 until dimension)
        for (column in 0 until dimension) {
            rowsSums[row] += board[row][column]
            columnsSums[column] += board[row][column]
        }
    var sol = 0
    for (row in 0 until dimension)
        for (column in 0 until dimension)
            if (columnsSums[column] > rowsSums[row]) sol++
    print(sol)
}