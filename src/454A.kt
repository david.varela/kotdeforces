fun main() {
    fun readInt() = readLine()!!.toInt()

    val n = readInt()
    var dSize = 1
    val sb = StringBuilder()
    repeat(n / 2) {
        repeat((n - dSize) / 2) { sb.append('*') }
        repeat(dSize) { sb.append('D') }
        repeat((n - dSize) / 2) { sb.append('*') }
        sb.append(System.lineSeparator())
        dSize+=2
    }
    repeat(n) { sb.append('D') }
    sb.append(System.lineSeparator())
    dSize = n - 2
    repeat(n / 2) {
        repeat((n - dSize) / 2) { sb.append('*') }
        repeat(dSize) { sb.append('D') }
        repeat((n - dSize) / 2) { sb.append('*') }
        sb.append(System.lineSeparator())
        dSize-=2
    }
    sb.deleteCharAt(sb.lastIndex)
    print(sb.toString())
}