fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val fly = readInts()
    val otherFly = readInts()
    for (pos in 0 until 3) if (fly[pos] == otherFly[pos]) return print("YES")
    print("NO")
}