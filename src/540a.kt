import kotlin.math.abs
import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()

    val numDisks = readInt()
    val original = readLine()!!
    val opens = readLine()!!
    var sol = 0
    for (pos in 0 until numDisks) {
        val n1 = original[pos].toInt()
        val n2 = opens[pos].toInt()
        sol += min(abs(n1 - n2), min(abs(n1 + 10 - n2), abs(n1 - 10 - n2)))
    }
    print(sol)
}