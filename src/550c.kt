fun main() {
    val s = "00" + readLine()!!
    for (pos1 in 0 until s.length - 2)
        for (pos2 in pos1 + 1 until s.length - 1)
            for (pos3 in pos2 + 1 until s.length) {
                val candidate = (s[pos1] - '0') * 100 + (s[pos2] - '0') * 10 + (s[pos3] - '0')
                if (candidate % 8 == 0) {
                    println("YES")
                    print(candidate)
                    return
                }
            }
    print("NO")
}