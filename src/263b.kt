fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numSquares, k) = readInts()
    val squares = readInts().sortedDescending()
    print(if (k > numSquares) -1 else "${squares[k - 1]} ${squares[k - 1]}")
}