import kotlin.math.max

fun main() {
    val (numCowBells, numBoxes) = readLine()!!.split(" ").map (String::toInt)
    val sizes = readLine()!!.split(" ").map (String::toInt)
    if (numCowBells <= numBoxes) return print(sizes.last())
    val extra = 2 * numBoxes - numCowBells
    var sol = if (extra == 0) 0 else sizes.subList(numCowBells - extra, numCowBells).maxOrNull()!!
    val right = numCowBells - extra - 1
    for (box in 0 until (numCowBells - extra) / 2)
        sol = max(sol, sizes[box] + sizes[right - box])
    print(sol)
}