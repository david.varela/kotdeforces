fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numApples, _, _) = readInts()
    val sol = IntArray(numApples)
    val arthurLikes = readInts().toSet()
    readLine()
    for (apple in 1..numApples)
        sol[apple - 1] = if (apple in arthurLikes) 1 else 2
    print(sol.joinToString(" "))
}