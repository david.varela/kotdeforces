fun main() {
    val s = readLine()!!
    val k = readLine()!!.toInt()
    val ws = readLine()!!.split(" ").map(String::toInt)
    val maxW = ws.maxOrNull()!!
    var sol = 0
    for ((pos, c) in s.withIndex()) sol += (pos + 1) * ws[c - 'a']
    for (pos in s.length + 1..s.length + k) sol += pos * maxW
    print(sol)
}