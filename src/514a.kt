fun main() {
    val x = readLine()!!.toCharArray()
    var first = true
    print(x.asSequence().map {
        if ((!first || it != '9') && it > '4') {
            first = false
            '9' - it
        } else {
            first = false
            it
        }
    }.joinToString(""))
}