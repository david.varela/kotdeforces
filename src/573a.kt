fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numBids = readInt()
    val bids = readInts().toIntArray()
    val visited = mutableSetOf<Int>()
    loop@ for (pos in 0 until numBids) {
        if (bids[pos] in visited) continue
        visited.add(bids[pos])
        while (bids[pos] % 2 == 0) {
            bids[pos] /= 2
            if (bids[pos] in visited) continue@loop
            visited.add(bids[pos])
        }
        while (bids[pos] % 3 == 0) {
            bids[pos] /= 3
            if (bids[pos] in visited) continue@loop
            visited.add(bids[pos])
        }
        if (bids[pos] != bids[0]) return print("No")
    }
    print("Yes")
}