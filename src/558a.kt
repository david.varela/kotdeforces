fun main() {
    val numTrees = readLine()!!.toInt()
    var numPositives = 0
    var numNegatives = 0
    val trees = mutableListOf<Pair<Int, Int>>()
    for (i in 1..numTrees) {
        val (pos, apples) = readLine()!!.split(" ").map { it.toInt() }
        if (pos > 0) numPositives++ else numNegatives++
        trees.add(pos to apples)
    }
    trees.sortBy { it.first }
    when {
        numNegatives == numPositives -> print(trees.sumBy { it.second })
        numNegatives > numPositives -> print(trees.slice(numNegatives - numPositives - 1 until numTrees).sumBy { it.second })
        else -> print(trees.slice(0..numTrees - numPositives + numNegatives).sumBy { it.second })
    }
}