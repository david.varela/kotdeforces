fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val repeats = intArrayOf(0, 0, 0)
    readLine()
    for ((pos, exercise) in readInts().withIndex())
        repeats[pos % 3] += exercise
    print(
        when {
            repeats[0] > repeats[1] && repeats[0] > repeats[2] -> "chest"
            repeats[1] > repeats[0] && repeats[1] > repeats[2] -> "biceps"
            else -> "back"
        }
    )
}