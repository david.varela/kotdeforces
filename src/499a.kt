fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numMoments, skipMinutes) = readInts()
    var sol = 0
    var moment = 1
    repeat(numMoments) {
        val (left, right) = readInts()
        val diff = left - moment
        sol += diff % skipMinutes + right - left + 1
        moment = right + 1
    }
    print(sol)
}