fun main() {
    fun readInt() = readLine()!!.toInt()

    val n = readInt()
    print(if (n % 2 == 0) "4 ${n - 4}" else "9 ${n - 9}")
}