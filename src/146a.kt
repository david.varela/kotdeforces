fun main() {
    val n = readLine()!!.toInt()
    val s = readLine()!!
    for (c in s) if (c != '4' && c != '7') return print("NO")
    print(if (s.substring(0, n / 2).count { it == '4' } == s.substring(n / 2, n).count { it == '4' }) "YES" else "NO")
}