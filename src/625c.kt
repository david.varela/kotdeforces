fun main() {
    val (size, selectedColumn) = readLine()!!.split(" ").map(String::toInt)
    val matrix = Array(size + 1) { IntArray(size + 1) }
    var value = 1
    for (column in 1 until selectedColumn) {
        for (row in 1..size) {
            matrix[row][column] = value
            value++
        }
    }
    var sol = 0
    for (row in 1..size) {
        sol += value
        for (column in selectedColumn..size) {
            matrix[row][column] = value
            value++
        }
    }
    println(sol)
    matrix.sliceArray(1..size).forEach { println(it.slice(1..size).joinToString(" ")) }
}