fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    val profits = readInts()
    val daysPerFolder = mutableListOf<Int>()
    var negatives = 0
    var days = 0
    for (profit in profits)
        if (profit < 0)
            if (negatives == 2) {
                daysPerFolder.add(days)
                negatives = 1
                days = 1
            } else {
                days++
                negatives++
            }
        else {
            days++
        }
    daysPerFolder.add(days)
    println(daysPerFolder.size)
    print(daysPerFolder.joinToString(" "))
}