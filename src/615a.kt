fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numButtons, numBulbs) = readInts()
    val bulbsOn = mutableSetOf<Int>()
    repeat(numButtons) {
        val b = readInts()
        bulbsOn.addAll(b.subList(1, b.size))
    }
    print(if (bulbsOn.size == numBulbs) "YES" else "NO")
}