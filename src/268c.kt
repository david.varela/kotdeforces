import kotlin.math.min

fun main() {
    val (xs, ys) = readLine()!!.split(" ").map(String::toInt)
    val min = min(xs, ys)
    println(min + 1)
    val sb = StringBuilder()
    for (pos in 0..min) sb.appendLine("$pos ${min - pos}")
    print(sb.toString())
}