fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (n1, n2, _, _) = readInts()
    print(if (n1 > n2) "First" else "Second")
}