import kotlin.math.max

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val n = readInt()
    val arr = readInts()
    var minPos = 0
    var maxPos = 0
    var startPos = 0
    var sol = 1
    for ((currentPos, current) in arr.withIndex()) {
        if (current <= arr[minPos]) minPos = currentPos
        if (current >= arr[maxPos]) maxPos = currentPos
        if (arr[maxPos] - arr[minPos] > 1) {
            sol = max(sol, currentPos - startPos)
            if (minPos < maxPos) {
                startPos = minPos + 1
                minPos = maxPos - 1
            } else {
                startPos = maxPos + 1
                maxPos = minPos - 1
            }
        }
    }
    print(max(sol, n - startPos))
}