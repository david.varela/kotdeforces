import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    var (_, increasedPoints) = readInts()
    val ys = readInts().toIntArray()
    for (point in 1 until ys.lastIndex) {
        val canReduce = ys[point] > (max(ys[point - 1], ys[point + 1]) + 1)
        if (canReduce) {
            ys[point]--
            increasedPoints--
            if (increasedPoints == 0) break
        }
    }
    print(ys.joinToString(" "))
}