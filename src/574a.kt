import java.util.*

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numCandidates = readInt()
    val votes = readInts()
    var limakVotes = votes[0]
    val opponentsVotes = PriorityQueue<Int>() { a, b -> b.compareTo(a) }
    opponentsVotes.addAll(votes.subList(1, numCandidates))
    var sol = 0
    while (limakVotes <= opponentsVotes.peek()) {
        sol++
        limakVotes++
        opponentsVotes.add(opponentsVotes.poll() - 1)
    }
    print(sol)
}