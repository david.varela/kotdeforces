fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val money = readInt()
    val guardPosts = mutableListOf<List<Int>>()
    for (i in 1..4) guardPosts.add(readInts())
    for (pos in 0 until 4)
        for (index1 in 0..1)
            for (index2 in 2..3)
                if (guardPosts[pos][index1] + guardPosts[pos][index2] <= money)
                    return print("${pos + 1} ${guardPosts[pos][index1]} ${money - guardPosts[pos][index1]}")
    print(-1)
}