import java.util.*
import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    fun BitSet.countConsecutive(): Int {
        var output = 0
        var count = 0
        for (pos in 0 until this.size()) {
            if (this[pos]) count++ else count = 0
            output = max(output, count)
        }
        return output
    }

    val (numRows, numCols, numRounds) = readInts()
    val grid = Array(numRows) { BitSet(numCols) }
    val sums = IntArray(numRows)
    val sols = IntArray(numRounds)
    for (numRow in 0 until numRows) {
        val row = readInts()
        for (numColumn in 0 until numCols)
            if (row[numColumn] == 1) grid[numRow].set(numColumn)
        sums[numRow] = grid[numRow].countConsecutive()
    }
    for (numRound in 0 until numRounds) {
        val (i, j) = readLine()!!.split(" ").map { it.toInt() - 1 }
        grid[i].flip(j)
        sums[i] = grid[i].countConsecutive()
        sols[numRound] = sums.maxOrNull()!!
    }
    print(sols.joinToString(System.lineSeparator()))
}