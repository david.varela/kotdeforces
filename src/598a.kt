fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readLong() = readLine()!!.toLong()

    val numQueries = readInt()
    for (query in 0 until numQueries) {
        val n = readLong()
        var toDeduct = 0L
        var powerOfTwo = 1L
        while (powerOfTwo <= n) {
            toDeduct += powerOfTwo
            powerOfTwo = powerOfTwo shl 1
        }
        println((n * (n + 1)) / 2 - 2 * (toDeduct))
    }
}