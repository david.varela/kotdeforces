fun main() {
    val (numRows, numCols) = readLine()!!.split(" ").map { it.toInt() }
    var blackRow = true
    for (r in 1..numRows) {
        var blackColumn = blackRow
        val row = readLine()!!.toCharArray()
        for (c in 0 until numCols) {
            if (row[c] == '.') {
                if (blackColumn) {
                    row[c] = 'B'
                } else {
                    row[c] = 'W'
                }
            }
            blackColumn = !blackColumn
        }
        println(row)
        blackRow = !blackRow
    }
}