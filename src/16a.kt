fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numRows, _) = readInts()
    val rows = CharArray(numRows)
    for (row in 0 until numRows) {
        val rowColors = readLine()!!.toSet()
        if (rowColors.size > 1) return print("NO")
        rows[row] = rowColors.first()
    }
    for (row in 1 until numRows - 1)
        if (rows[row - 1] == rows[row] || rows[row] == rows[row + 1]) return print("NO")
    print("YES")
}