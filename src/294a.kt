fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numWires = readInt()
    val birds = readInts().toIntArray()
    repeat(readInt()) {
        val (numWire, numBird) = readInts()
        if (numWire > 1) birds[numWire - 2] += numBird - 1
        if (numWire < numWires) birds[numWire] += birds[numWire - 1] - numBird
        birds[numWire - 1] = 0
    }
    print(birds.joinToString(System.lineSeparator()))
}