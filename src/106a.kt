fun main() {
    val cardToValue =
        mapOf('6' to 6, '7' to 7, '8' to 8, '9' to 9, 'T' to 10, 'J' to 11, 'Q' to 12, 'K' to 13, 'A' to 14)
    val trump = readLine()!![0]
    val (first, second) = readLine()!!.split(" ")
    if (first[1] == second[1] && cardToValue[first[0]]!! > cardToValue[second[0]]!!) return print("YES")
    if (first[1] == trump && second[1] != trump) return print("YES")
    print("NO")
}