fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    print(
        when (readInts().maxOrNull()!!) {
            1 -> "1/1"
            2 -> "5/6"
            3 -> "2/3"
            4 -> "1/2"
            5 -> "1/3"
            else -> "1/6"
        }
    )
}