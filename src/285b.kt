fun main() {
    var (_, start, end) = readLine()!!.split(" ").map(String::toInt)
    val visited = mutableSetOf<Int>()
    val glasses = mutableListOf(0)
    glasses.addAll(readLine()!!.split(" ").map(String::toInt))
    var output = 0
    while (start != end) {
        if (start in visited) {
            output = -1
            break
        } else {
            visited.add(start)
            start = glasses[start]
            output++
        }
    }
    print(output)
}