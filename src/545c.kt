fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numTrees = readInt()
    val positions = IntArray(numTrees)
    val heights = IntArray(numTrees)
    for (pos in 0 until numTrees) {
        val (treePosition, height) = readInts()
        positions[pos] = treePosition
        heights[pos] = height
    }
    var sol = if (numTrees == 1) 1 else 2
    for (pos in 1 until numTrees - 1)
        if (positions[pos] - heights[pos] > positions[pos - 1]) sol++
        else if (positions[pos] + heights[pos] < positions[pos + 1]) {
            sol++
            positions[pos] += heights[pos]
        }
    print(sol)
}