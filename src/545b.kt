fun main() {
    val s = readLine()!!
    val t = readLine()!!
    var evenDistance = true
    var sTurn = true
    val sb = StringBuilder()
    for (pos in s.indices) {
        if (s[pos] != t[pos]) {
            evenDistance = !evenDistance
            if (sTurn) sb.append(s[pos]) else sb.append(t[pos])
            sTurn = !sTurn
        } else {
            sb.append(s[pos])
        }
    }
    if (evenDistance) {
        print(sb.toString())
    } else {
        print("impossible")
    }
}
