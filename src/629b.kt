import kotlin.math.max
import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()

    val numFriends = readInt()
    val malesCounter = IntArray(368)
    val femalesCounter = IntArray(368)
    repeat(numFriends) {
        val (sex, from, to) = readLine()!!.split(" ")
        val counter = if (sex == "M") malesCounter else femalesCounter
        counter[from.toInt()]++
        counter[to.toInt() + 1]--
    }
    var cars = 0
    var females = 0
    var males = 0
    for (day in 1..366) {
        males += malesCounter[day]
        females += femalesCounter[day]
        cars = max(cars, min(males, females))
    }
    print(2 * cars)
}