import kotlin.math.max
import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (p, q, l, r) = readInts()
    val ps = Array(p) { 0 to 0 }
    val qs = Array(q) { 0 to 0 }
    for (i in 0 until p) {
        val (start, end) = readInts()
        ps[i] = start to end
    }
    for (i in 0 until q) {
        val (start, end) = readInts()
        qs[i] = start to end
    }
    val sols = mutableSetOf<Int>()
    for (cp in ps)
        for (cq in qs) {
            val left = max(cp.first - cq.second, l)
            val right = min(cp.second - cq.first, r)
            for (i in left..right) sols.add(i)
        }
    print(sols.size)
}