import kotlin.math.max

fun main() {
    val (numTypesOfSugar, dollars) = readLine()!!.split(" ").map(String::toInt)
    var sweets = -1
    for (i in 1..numTypesOfSugar) {
        val (sugarDollars, sugarCents) = readLine()!!.split(" ").map(String::toInt)
        if (dollars > sugarDollars || (dollars == sugarDollars && sugarCents == 0))
            sweets = max(sweets, (100 - sugarCents) % 100)
    }
    print(sweets)
}