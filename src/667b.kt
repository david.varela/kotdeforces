fun main() {
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    readLine()
    val lengths = readLongs()
    val longest = lengths.maxOrNull()!!
    print(longest - (lengths.sum() - longest) + 1L)
}