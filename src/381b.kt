fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    val cards = readInts()
    val times = mutableMapOf<Int, Int>().withDefault { 0 }
    for (card in cards) times[card] = times.getValue(card) + 1
    val sortedCards = times.toList().sortedBy { it.first }
    val sol = mutableListOf<Int>()
    for (card in sortedCards) sol.add(card.first)
    for (pos in sortedCards.lastIndex - 1 downTo 0) if (sortedCards[pos].second > 1) sol.add(sortedCards[pos].first)
    println(sol.size)
    print(sol.joinToString(" "))
}