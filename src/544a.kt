fun main() {
    fun readInt() = readLine()!!.toInt()

    val numSubStrings = readInt()
    val s = readLine()!!
    val visited = mutableSetOf<Char>()
    val solution = mutableListOf<Int>()
    for ((pos, c) in s.withIndex())
        if (c !in visited) {
            visited.add(c)
            solution.add(pos)
            if (solution.size == numSubStrings) break
        }
    if (solution.size < numSubStrings) return print("NO")
    println("YES")
    solution.add(s.length)
    for (pos in 0 until solution.size - 1)
        println(s.substring(solution[pos], solution[pos + 1]))
}