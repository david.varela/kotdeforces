fun main() {
    val (numPeople, elevatorCapacity) = readLine()!!.split(" ").map(String::toInt)
    val floors = readLine()!!.split(" ").map(String::toInt).sortedDescending()
    var pos = 0
    var sol = 0
    while (pos < numPeople) {
        sol += 2 * (floors[pos] - 1)
        pos += elevatorCapacity
    }
    print(sol)
}