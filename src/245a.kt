fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val counts = intArrayOf(0, 0, 0)
    val sums = intArrayOf(0, 0, 0)
    val n = readInt()
    repeat(n) {
        val (pos, x, _) = readInts()
        counts[pos]++
        sums[pos] += x
    }
    for (pos in 1..2)
        println(if (5 * counts[pos] <= sums[pos]) "LIVE" else "DEAD")
}