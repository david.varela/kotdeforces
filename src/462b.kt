import java.math.BigInteger
import kotlin.math.min

fun main() {
    var (_, numChoose) = readLine()!!.split(" ").map(String::toInt)
    val cards = readLine()!!
    val freqs = mutableMapOf(*('A'..'Z').map { Pair(it, 0) }.toList().toTypedArray())
    for(c in cards) freqs[c] = freqs[c]!! + 1
    val sorted = freqs.toList().sortedByDescending { it.second }
    var sol = BigInteger.ZERO
    for(freq in sorted) {
        val numSame = min(numChoose, freq.second)
        sol = sol.add(BigInteger.valueOf(numSame * numSame.toLong()))
        numChoose -= numSame
        if(numChoose == 0) break
    }
    print(sol)
}