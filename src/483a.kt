fun main() {
    val (l, r) = readLine()!!.split(" ").map(String::toLong)
    if (l % 2L == 0L) return print(if (l + 2 > r) "-1" else "$l ${l + 1} ${l + 2}")
    print(if (l + 3 > r) "-1" else "${l + 1} ${l + 2} ${l + 3}")
}