import java.io.File

fun main() {
    val lines = File("input.txt").readLines()
    val (_, readHours) = lines[0].split(" ").map(String::toInt)
    val data = lines[1].split(" ").map(String::toInt).mapIndexed { index, i -> i to index + 1 }
        .sortedByDescending { it.first }.subList(0, readHours)
    val sol = "${data[readHours - 1].first}${System.lineSeparator()}${data.joinToString(" ") { it.second.toString() }}"
    File("output.txt").writeText(sol)
}