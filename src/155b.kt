fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val cards = mutableListOf<Pair<Int, Int>>()
    repeat(readInt()) {
        val (points, extraCards) = readInts()
        cards.add(points to extraCards)
    }
    cards.sortWith(compareBy({ -it.second }, { -it.first }))
    var numCards = 1
    var points = 0
    for (card in cards)
        if (numCards == 0) break else {
            numCards += card.second - 1
            points += card.first
        }
    print(points)
}