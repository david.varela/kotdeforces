fun main() {
    val n = readLine()!!.toInt()
    var movement = 1
    while (movement shl 1 < n) movement = movement shl 1
    val sols = LongArray(n + 1)
    val arr = readLine()!!.split(" ").map(String::toLong).toLongArray()
    for (pos in 1 until n) {
        sols[pos] = sols[pos - 1] + arr[pos - 1]
        while (pos + movement > n) movement /= 2
        arr[pos + movement - 1] += arr[pos - 1]
    }
    print(sols.slice(1 until n).joinToString(System.lineSeparator()))
}