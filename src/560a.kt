fun main() {
    readLine()
    val values = readLine()!!.split(" ").map(String::toInt)
    print(if (1 in values) -1 else 1)
}