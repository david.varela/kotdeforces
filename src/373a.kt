fun main() {
    val k = 2 * readLine()!!.toInt()
    val occs = mutableMapOf<Char, Int>().withDefault { 0 }
    repeat(4) {
        for (c in readLine()!!)
            if (c != '.') occs[c] = occs.getValue(c) + 1
    }
    print(if(occs.isEmpty() || occs.values.maxOrNull()!! <= k) "YES" else "NO")
}