import kotlin.math.abs
import kotlin.math.max

fun main() {
    val from = readLine()!!
    val to = readLine()!!
    var horizontal = from[0] - to[0]
    var vertical = from[1] - to[1]
    println(max(abs(horizontal), abs(vertical)))
    while (horizontal != 0 || vertical != 0) {
        when {
            horizontal < 0 -> {
                print('R'); horizontal++
            }
            horizontal > 0 -> {
                print('L'); horizontal--
            }
        }
        when {
            vertical < 0 -> {
                print('U'); vertical++
            }
            vertical > 0 -> {
                print('D'); vertical--
            }
        }
        println()
    }
}