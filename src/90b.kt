fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numRows, numColumns) = readInts()
    val grid = Array(numRows) { "" }
    val crossed = Array(numRows) { BooleanArray(numColumns) }
    for (row in 0 until numRows) grid[row] = readLine()!!
    val charToRowByColumn = Array(numColumns) { mutableMapOf<Char, Int>() }
    for ((numRow, row) in grid.withIndex()) {
        val charToColumn = mutableMapOf<Char, Int>()
        for ((numColumn, c) in row.withIndex()) {
            if (c in charToColumn) {
                crossed[numRow][numColumn] = true
                crossed[numRow][charToColumn[c]!!] = true
            } else
                charToColumn[c] = numColumn

            if (c in charToRowByColumn[numColumn]) {
                crossed[numRow][numColumn] = true
                crossed[charToRowByColumn[numColumn][c]!!][numColumn] = true
            } else
                charToRowByColumn[numColumn][c] = numRow
        }
    }
    val sb = StringBuilder()
    for (numRow in 0 until numRows)
        for (numColumn in 0 until numColumns)
            if (!crossed[numRow][numColumn]) sb.append(grid[numRow][numColumn])
    print(sb.toString())
}