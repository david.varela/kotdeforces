import kotlin.math.abs

fun main() {
    val (a, b, s) = readLine()!!.split(" ").map(String::toLong)
    val distance = abs(a) + abs(b)
    print(if (s >= distance && (s - distance) % 2 == 0L) "Yes" else "No")
}