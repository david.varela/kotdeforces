fun main() {
    val scale = readLine()!!
    val middle = scale.indexOf('|')
    val left = scale.substring(0, middle)
    val right = scale.substring(middle + 1)
    val (smaller, bigger) = if (left.length <= right.length) left to right else right to left
    val other = readLine()!!
    if ((smaller.length + bigger.length + other.length) and (1) == 1) return print("Impossible")
    val half = (smaller.length + bigger.length + other.length) / 2
    if (smaller.length + other.length < half) return print("Impossible")
    val newSmaller = smaller + other.substring(0, half - smaller.length)
    val newBigger = bigger + other.substring(half - smaller.length)
    print(if (left.length <= right.length) "$newSmaller|$newBigger" else "$newBigger|$newSmaller")
}