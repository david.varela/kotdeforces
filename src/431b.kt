import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    val used = BooleanArray(5)
    var sol = 0
    val permutation = mutableListOf<Int>()
    val happiness = Array(5) { IntArray(5) }

    fun permute() {
        if (permutation.size == 5) {
            var oneSol = 0
            oneSol += happiness[permutation[0]][permutation[1]] + happiness[permutation[1]][permutation[0]]
            oneSol += happiness[permutation[2]][permutation[3]] + happiness[permutation[3]][permutation[2]]
            oneSol += happiness[permutation[1]][permutation[2]] + happiness[permutation[2]][permutation[1]]
            oneSol += happiness[permutation[3]][permutation[4]] + happiness[permutation[4]][permutation[3]]
            oneSol += happiness[permutation[2]][permutation[3]] + happiness[permutation[3]][permutation[2]]
            oneSol += happiness[permutation[3]][permutation[4]] + happiness[permutation[4]][permutation[3]]
            sol = max(sol, oneSol)
        } else {
            for (pos in 0..4) {
                if (used[pos]) continue
                used[pos] = true
                permutation.add(pos)
                permute()
                permutation.removeAt(permutation.lastIndex)
                used[pos] = false
            }
        }
    }

    for (row in 0 until 5) happiness[row] = readInts().toIntArray()
    permute()
    print(sol)
}