fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val n = readInt()
    val grid = Array(n) { IntArray(n) }
    for (row in 0 until n) grid[row] = readInts().toIntArray()
    val sol = IntArray(n)
    val used = mutableSetOf<Int>()
    for ((numRow, row) in grid.withIndex()) {
        if (sol[numRow] != 0) continue
        val inRow = mutableSetOf<Int>()
        for (num in row)
            if (num in inRow) {
                sol[numRow] = num
                used.add(num)
            } else inRow.add(num)
    }
    print(sol.joinToString(" "))
}