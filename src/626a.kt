fun main() {
    val visited = mutableMapOf<Pair<Int, Int>, Int>().withDefault { 0 }
    visited[0 to 0] = 1
    val instructionToMovement = mapOf('U' to (0 to 1), 'D' to (0 to -1), 'L' to (-1 to 0), 'R' to (1 to 0))
    readLine()
    var sol = 0
    var pos = 0 to 0
    for (c in readLine()!!) {
        val movement = instructionToMovement[c]!!
        pos = pos.first + movement.first to pos.second + movement.second
        sol += visited.getValue(pos)
        visited[pos] = visited.getValue(pos) + 1
    }
    print(sol)
}