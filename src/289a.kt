fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numSegments, divisor) = readInts()
    var sum = 0
    repeat(numSegments) {
        val (left, right) = readInts()
        sum += right - left + 1
    }
    print(if (sum % divisor == 0) 0 else divisor - sum % divisor)
}