fun main() {
    fun readInt() = readLine()!!.toInt()

    val n = readInt()
    print(2 * (n / 3) + if (n % 3 == 0) 0 else 1)
}