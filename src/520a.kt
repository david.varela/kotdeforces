fun main() {
    readLine()
    val s = readLine()!!
    val chars = mutableSetOf<Char>()
    for (c in s) chars.add(c.toLowerCase())
    print(if (chars.size == 26) "YES" else "NO")
}