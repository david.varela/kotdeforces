fun main() {
    val (x, y) = readLine()!!.split(" ").map { it.toLong() }
    when {
        x > 0 && y > 0 -> print("0 ${x + y} ${x + y} 0")
        x < 0 && y > 0 -> print("${x - y} 0 0 ${-x + y}")
        x < 0 && y < 0 -> print("${x + y} 0 0 ${x + y}")
        x > 0 && y < 0 -> print("0 ${-x + y} ${x - y} 0")
    }
}