fun main() {
    val results = mutableSetOf<Int>()
    for (n in 1..35)
        results.add(n * (n + 1) / 2)
    print(if (readLine()!!.toInt() in results) "YES" else "NO")
}