fun main() {
    val s = readLine()!!
    val numFours = s.count { it == '4' }
    val numSevens = s.count { it == '7' }
    print(
        when {
            numFours == 0 && numSevens == 0 -> -1
            numFours >= numSevens -> 4
            else -> 7
        }
    )
}