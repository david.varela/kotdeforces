import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numDays, numKilos) = readInts()
    val prices = readInts()
    var best = 0
    for (day in 0 until numDays - 1)
        best = max(best, prices[day] - prices[day + 1] - numKilos)
    print(best)
}