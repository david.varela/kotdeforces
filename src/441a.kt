fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numSellers, money) = readInts()
    val sellersToDeal = mutableListOf<Int>()
    for (seller in 1..numSellers) {
        val info = readInts()
        if (info.subList(1, info.size).minOrNull()!! < money) sellersToDeal.add(seller)
    }
    println(sellersToDeal.size)
    print(sellersToDeal.joinToString(" "))
}