fun main() {
    val s = readLine()!!
    val sb = StringBuilder()
    val startDomain = if (s.first() == 'h') {
        sb.append("http://")
        4
    } else {
        sb.append("ftp://")
        3
    }
    val domainEnd = s.indexOf("ru", startDomain + 1)
    sb.append(s.substring(startDomain, domainEnd))
    sb.append(".ru")
    if (domainEnd + 2 != s.length) sb.append("/${s.substring(domainEnd + 2)}")
    print(sb.toString())
}