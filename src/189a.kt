import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (n, a, b, c) = readInts()
    val dp = IntArray(n + 1) { Int.MIN_VALUE }
    dp[0] = 0
    for (pos in 0 .. n) {
        if (pos - a >= 0) dp[pos] = max(dp[pos], dp[pos - a] + 1)
        if (pos - b >= 0) dp[pos] = max(dp[pos], dp[pos - b] + 1)
        if (pos - c >= 0) dp[pos] = max(dp[pos], dp[pos - c] + 1)
    }
    print(dp[n])
}