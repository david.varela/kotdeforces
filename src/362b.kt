fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numStairs, numDirtyStairs) = readInts()
    if (numDirtyStairs == 0) return print("YES")
    val dirty = readInts().sorted()
    if (dirty[0] == 1) return print("NO")
    if (dirty.last() == numStairs) return print("NO")
    for (pos in 2 until dirty.size)
        if (dirty[pos - 2] == dirty[pos] - 2) return print("NO")
    print("YES")
}