import kotlin.math.max
import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numHolds = readInt()
    val heights = readInts()
    var sol = Int.MAX_VALUE
    for (pos in 1 until numHolds - 1) sol = min(sol, heights[pos + 1] - heights[pos - 1])
    for (pos in 1 until numHolds) sol = max(sol, heights[pos] - heights[pos - 1])
    print(sol)
}