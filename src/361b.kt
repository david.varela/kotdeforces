fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (n, k) = readInts()
    if (n == k) return print(-1)
    if (n - 1 == k) return print((1..n).joinToString(" "))
    val sb = StringBuilder()
    sb.append("${k + 2} ")
    for (i in 2..k + 1) sb.append("$i ")
    for (i in k + 3..n) sb.append("$i ")
    sb.append("1")
    print(sb)
}