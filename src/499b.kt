fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (_, numWordsDict) = readInts()
    val dict = mutableMapOf<String, String>()
    repeat(numWordsDict) {
        val (s1, s2) = readLine()!!.split(" ")
        dict[s1] = if (s1.length <= s2.length) s1 else s2
    }
    print(readLine()!!.split(" ").joinToString(" ") { s -> dict[s]!! })
}