import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    data class Friend(val money: Long = 0, val friendship: Long = 0)

    val (numFriends, d) = readInts()
    val friends = Array(numFriends) { Friend() }
    for (pos in 0 until numFriends) {
        val (money, friendship) = readLongs()
        friends[pos] = Friend(money, friendship)
    }
    friends.sortBy(Friend::money)
    var maxFriendship = Long.MIN_VALUE
    var currentFriendship = 0L
    var left = 0
    for (rightFriend in friends) {
        if (rightFriend.money - friends[left].money >= d) {
            maxFriendship = max(maxFriendship, currentFriendship)
            while (rightFriend.money - friends[left].money >= d) {
                currentFriendship -= friends[left].friendship
                left++
            }
        }
        currentFriendship += rightFriend.friendship
    }
    print(max(maxFriendship, currentFriendship))
}