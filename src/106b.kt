fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numLaptops = readInt()
    val laptops = Array(numLaptops) { IntArray(4) }
    for (laptop in 0 until numLaptops) laptops[laptop] = readInts().toIntArray()
    nextCandidate@ for (candidate in laptops)
        for (otherLaptop in laptops)
            if (candidate[0] < otherLaptop[0] && candidate[1] < otherLaptop[1] && candidate[2] < otherLaptop[2]) {
                candidate[3] = Int.MAX_VALUE
                continue@nextCandidate
            }
    print(laptops.withIndex().minByOrNull { it.value[3] }!!.index + 1)
}