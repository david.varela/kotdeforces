fun main() {
    val s = readLine()!!
    var start = -1
    var sol = 0
    for (pos in 3 until s.length) {
        if (s.substring(pos - 3..pos) == "bear")
            start = pos - 3
        if (start != -1)
            sol += start + 1
    }
    print(sol)
}