fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (height, width) = readInts()
    val grid = Array(height) {""}
    for (pos in 0 until height) grid[pos] = readLine()!!
    var sol = 0
    val goal = setOf('f', 'a', 'c', 'e')
    for (row in 0 until height - 1)
        for (column in 0 until width - 1) {
            val faceSet = setOf(grid[row][column], grid[row][column + 1], grid[row + 1][column], grid[row + 1][column + 1])
            if (faceSet.intersect(goal).size == 4) sol++
        }
    print(sol)
}