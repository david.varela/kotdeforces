fun main() {
    val n = readLine()!!.toInt()
    val numToText = listOf("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
        "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen")
    val tens = listOf("", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety")
    print(
        when {
            n < 20 -> numToText[n]
            n % 10 == 0 -> tens[n / 10]
            else -> tens[n / 10] + '-' + numToText[n % 10]
        }
    )
}
