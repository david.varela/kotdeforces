fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (length, k) = readInts()
    val sb = StringBuilder()
    for (i in k + 1 downTo 1) sb.append("$i ")
    for (i in k + 2..length) sb.append("$i ")
    print(sb.toString())
}