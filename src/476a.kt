fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (n, m) = readInts()
    val a = n / 2
    val b = n % 2
    var extras = (a + b) % m
    if (extras != 0) extras = m - extras
    print (if (extras > a) -1 else a + b + extras)
}