import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numStations = readInt()
    val distances = readInts()
    val (from, to) = readLine()!!.split(" ").map { it.toInt() - 1 }
    var sol1 = 0
    var sol2 = 0
    var pos = from
    while (pos != to) {
        sol1 += distances[pos]
        pos = (pos + 1) % numStations
    }
    while (pos != from) {
        sol2 += distances[pos]
        pos = (pos + 1) % numStations
    }
    print(min(sol1, sol2))
}