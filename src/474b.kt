fun main() {
    val numPiles = readLine()!!.toInt()
    val topPiles = LongArray(numPiles + 2)
    val pilesHeights = readLine()!!.split(' ').map { it.toLong() }
    topPiles[0] = 0; topPiles[1] = pilesHeights[0]; topPiles[numPiles + 1] = Long.MAX_VALUE
    for (pos in 1 until numPiles) {
        topPiles[pos + 1] = topPiles[pos] + pilesHeights[pos]
    }
    readLine()
    readLine()!!.split(' ').map { it.toLong() }.forEach {
        var result = topPiles.binarySearch(it)
        if (result < 0) result = (result + 1) * -1
        println(result)
    }
}