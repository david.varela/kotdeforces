fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readLong() = readLine()!!.toLong()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    val (numRows, numcolumns) = readInts()
    val sb = StringBuilder()
    var left = false
    for (row in 1 until numRows) {
        if (row % 2 == 1) {
            for (c in 1..numcolumns)
                sb.append('#')
        } else {
            if (left) sb.append('#')
            for (c in 1 until numcolumns)
                sb.append('.')
            if (!left) sb.append('#')
            left = !left
        }
        sb.append(System.lineSeparator())
    }
    for (c in 1..numcolumns)
        sb.append('#')
    print(sb.toString())
}