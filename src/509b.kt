fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (_, k) = readInts()
    val arr = readInts()
    val min = arr.minOrNull()!!
    val max = arr.maxOrNull()!!
    if ((max / k - 1 < min / k) || ((max / k - 1 == min / k) && (min % k) >= max % k)) {
        println("YES")
        val sb = StringBuilder()
        for (a in arr) {
            for (p in 0 until a) {
                sb.append("${p % k + 1} ")
            }
            sb.append(System.lineSeparator())
        }
        print(sb.toString())
    } else
        print("NO")
}