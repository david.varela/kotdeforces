import java.util.*

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numVertices, numCats) = readInts()
    val cats = mutableListOf(0)
    cats.addAll(readInts())
    val edges = Array<MutableSet<Int>>(numVertices + 1) { mutableSetOf() }
    repeat(numVertices - 1) {
        val (x, y) = readInts()
        edges[x].add(y)
        edges[y].add(x)
    }
    val visited = mutableSetOf(1)
    val queue = ArrayDeque<Pair<Int, Int>>()
    for (edge in edges[1]) queue.add(edge to cats[1])
    var sol = 0
    while (queue.isNotEmpty()) {
        var (vertex, consecutiveCats) = queue.poll()
        visited.add(vertex)
        if (consecutiveCats + cats[vertex] <= numCats) {
//            consecutiveCats = (consecutiveCats + 1) * cats[vertex]
            consecutiveCats = if (cats[vertex] == 0) 0 else consecutiveCats + 1
            var leaf = true
            for (v in edges[vertex]) if (v !in visited) {
                queue.add(v to consecutiveCats)
                leaf = false
            }
            if (leaf) sol++
        }
    }
    print(sol)
}