fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (a, b, n) = readInts()
    for (candidate in 0..9) {
        val aa = a * 10 + candidate
        if (aa % b == 0) {
            val sb = StringBuilder()
            sb.append(aa)
            repeat(n - 1) { sb.append('0') }
            return print(sb.toString())
        }
    }
    print(-1)
}