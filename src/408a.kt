import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numCashiers = readInt()
    readLine()
    var sol = Int.MAX_VALUE
    repeat(numCashiers) {
        val numProducts = readInts()
        sol = min(sol, numProducts.sum() * 5 + numProducts.size * 15)
    }
    print(sol)
}