fun main() {
    readLine()
    val horizontals = readLine()!!
    val verticals = readLine()!!
    if (horizontals.first() == '>' &&
        verticals.last() == 'v' &&
        horizontals.last() == '<'
        && verticals.first() == '^'
    )
        return print("YES")
    if (horizontals.first() == '<' &&
        verticals.last() == '^' &&
        horizontals.last() == '>'
        && verticals.first() == 'v'
    )
        return print("YES")
    print("NO")
}