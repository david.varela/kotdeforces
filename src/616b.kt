fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numRows, numColumns) = readInts()
    val matrix = Array(numRows) { List<Int>(numColumns) { 0 } }
    for (row in 0 until numRows) matrix[row] = readInts()
    print(matrix.map { it.minOrNull()!! }.maxOrNull()!!)
}