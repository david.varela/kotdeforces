fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (r1, r2) = readInts()
    val (c1, c2) = readInts()
    val (d1, d2) = readInts()
    nextAttempt@ for (tl in 1..9) {
        val tr = r1 - tl
        val bl = c1 - tl
        val br = d1 - tl
        val all = setOf(tl, tr, bl, br)
        if (all.size != 4) continue
        for (gem in all) if (gem !in 1..9) continue@nextAttempt
        if (r2 != bl + br) continue
        if (c2 != tr + br) continue
        if (d2 != tr + bl) continue
        println("$tl $tr")
        return print("$bl $br")
    }
    print(-1)
}