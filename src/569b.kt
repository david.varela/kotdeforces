fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts(numItems: Int): IntArray {
        val strings = readLine()!!.split(" ")
        val output = IntArray(numItems)
        for ((pos, text) in strings.withIndex()) output[pos] = text.toInt()
        return output
    }

    val numItems = readInt()
    val isReserved = BooleanArray(numItems + 1)
    val ids = readInts(numItems)
    for ((pos, id) in ids.withIndex())
        if (id > numItems || isReserved[id])
            ids[pos] = Int.MAX_VALUE
        else
            isReserved[id] = true
    val sb = StringBuilder()
    var available = 1
    while (available < numItems && isReserved[available]) available++
    for (index in ids)
        if (index <= numItems) sb.append("$index ") else {
            sb.append("$available ")
            do available++ while (available < numItems && isReserved[available])
        }
    print(sb)
}