fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (d, l, v1, v2) = readInts()
    print((l - d) / (v1 + v2).toDouble())
}