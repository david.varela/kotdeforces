import kotlin.math.sqrt

fun main() {
    fun readLong() = readLine()!!.toLong()

    fun Long.factors(): MutableSet<Long> {
        val output = mutableSetOf<Long>()
        val endLoop = sqrt(this.toDouble()).toLong()
        for (i in 1L..endLoop) {
            if (this % i == 0L) {
                output.add(i)
                output.add(this / i)
            }
        }
        return output
    }

    val n = readLong()
    val factors = n.factors().sortedDescending()
    loop@ for (factor in factors) {
        val subFactors = factor.factors()
        subFactors.remove(1L)
        for (subFactor in subFactors) {
            val a = sqrt(subFactor.toDouble()).toLong()
            if (a * a == subFactor) continue@loop
        }
        print(factor)
        return
    }
    print(1)
}