fun main() {
    val (numRows, numCols) = readLine()!!.split(" ").map(String::toInt)
    val board = Array(numRows + 2) { ArrayList<Char>(numCols + 2) }
    board[0] = arrayListOf(*(1..numCols + 2).map { '.' }.toTypedArray())
    for (i in 1..numRows) {
        val l = arrayListOf('.')
        l.addAll(readLine()!!.toCharArray().toList())
        l.add('.')
        board[i] = l
    }
    board[numRows + 1] = arrayListOf(*(1..numCols + 2).map { '.' }.toTypedArray())
    var sol = 0
    for (row in 1..numRows)
        for (col in 1..numCols)
            if (board[row][col] == 'P')
                when {
                    board[row - 1][col] == 'W' -> {
                        sol++
                        board[row - 1][col] = '.'
                    }
                    board[row][col - 1] == 'W' -> {
                        sol++
                        board[row][col - 1] = '.'
                    }
                    board[row][col + 1] == 'W' -> {
                        sol++
                        board[row][col + 1] = '.'
                    }
                    board[row + 1][col] == 'W' -> {
                        sol++
                        board[row + 1][col] = '.'
                    }
                }
    print(sol)
}