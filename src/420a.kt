fun main() {
    val valids = setOf('A', 'H', 'I', 'M', 'O', 'T', 'U', 'V', 'W', 'X', 'Y')
    val s = readLine()!!
    for (c in s) if (c !in valids) return print("NO")
    if (s != s.reversed()) return print("NO")
    print("YES")
}