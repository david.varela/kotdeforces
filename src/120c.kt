import java.io.File
import kotlin.math.max

fun main() {
    val lines = File("input.txt").readLines()

    val (_, k) = lines[0].split(" ").map(String::toInt)
    val jars = lines[1].split(" ").map(String::toInt)
    var sol = 0
    for (jar in jars)
        sol += max(jar - 3*k, jar % k)
    File("output.txt").writeText(sol.toString())
}