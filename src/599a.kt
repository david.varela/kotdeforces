import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (d1, d2, d3) = readInts()
    print(min(d1 + d2 + d3, min(d1 + d1 + d2 + d2, min(d1 + d1 + d3 + d3, d2 + d2 + d3 + d3))))
}