fun main() {
    fun readInt() = readLine()!!.toInt()

    val numDrives = readInt()
    val size = readInt()
    val drivesSizes = mutableListOf<Int>()
    repeat(numDrives) {
        drivesSizes.add(readInt())
    }
    drivesSizes.sortDescending()
    var sol = 0
    for ((pos, drive) in drivesSizes.withIndex()) {
        sol += drive
        if (sol >= size) return print(pos + 1)
    }
}