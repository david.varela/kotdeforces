import kotlin.math.min

fun main() {
    val board = Array(8) { CharArray(8) }
    for (numRow in 0 until 8)
        board[numRow] = readLine()!!.toCharArray()
    var bestB = Int.MAX_VALUE
    var bestW = Int.MAX_VALUE
    for (row in 0 until 8)
        loop@ for (column in 0 until 8) {
            when (board[row][column]) {
                'B' -> {
                    for (rowCheck in row + 1 until 8)
                        if (board[rowCheck][column] == 'W') continue@loop
                    bestB = min(bestB, 7 - row)
                }
                'W' -> {
                    for (rowCheck in row - 1 downTo 0)
                        if (board[rowCheck][column] == 'B') continue@loop
                    bestW = min(bestW, row)
                }
            }
        }
    print(if (bestB < bestW) 'B' else 'A')
}