fun main() {
    val long = readLine()!!
    val short = readLine()!!
    var sol = 0
    var shortPos = 0
    var longPos = 0
    while (longPos < long.length) {
        if (long[longPos] == short[shortPos]) {
            shortPos++
            if (shortPos == short.length) {
                sol++
                shortPos = 0
            }
        } else {
            longPos -= shortPos
            shortPos = 0
        }
        longPos++
    }
    print(sol)
}