import kotlin.math.max

fun main() {
    var (numBooks, numMinutes) = readLine()!!.split(" ").map(String::toInt)
    val times = readLine()!!.split(" ").map(String::toInt).toMutableList()
    times.add(0)
    var start = 0
    var end = 0
    numMinutes -= times[0]
    var best = 0
    while (end < numBooks) {
        if (numMinutes < 0) {
            best = max (best, end - start)
            numMinutes += times[start]
            start++
        } else {
            end++
            numMinutes -= times[end]
        }
    }
    if (numMinutes >= 0) {
        best = max (best, end - start)
    }
    print(best)
}