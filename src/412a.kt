fun main() {
    val (numCharacters, ladderPosition) = readLine()!!.split(" ").map(String::toInt)
    val slogan = readLine()!!
    if (ladderPosition > numCharacters / 2) {
        repeat(numCharacters - ladderPosition) { println("RIGHT") }
        for (pos in numCharacters - 1 downTo 1) {
            println("PRINT ${slogan[pos]}")
            println("LEFT")
        }
        print("PRINT ${slogan.first()}")
    } else {
        repeat(ladderPosition - 1) { println("LEFT") }
        for (pos in 0 until numCharacters - 1) {
            println("PRINT ${slogan[pos]}")
            println("RIGHT")
        }
        print("PRINT ${slogan.last()}")
    }

}