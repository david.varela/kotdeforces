import kotlin.math.max

fun main() {
    val (numCitizens, numWizards, percentage) = readLine()!!.split(" ").map(String::toInt)
    val total = numCitizens * percentage / 100 + if ((numCitizens * percentage) % 100 == 0) 0 else 1
    print(max(0, total - numWizards))
}