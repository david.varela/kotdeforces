fun main() {
    val numHouses = readLine()!!.toInt()
    val houses = readLine()!!.split(" ").map { it.toInt() }
    val result = IntArray(numHouses) { 0 }
    var maxHeight = houses.last()
    for (i in numHouses - 2 downTo 0) {
        if (houses[i] > maxHeight) {
            maxHeight = houses[i]
        } else {
            result[i] = maxHeight + 1 - houses[i]
        }
    }
    print(result.joinToString(" "))
}