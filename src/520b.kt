/*
* To reach an odd number, it's necessary to come from the next even number, odd + 1. The reason is that it's impossible
* to reach a number like 11 from its half, 5.5, so it's necessary to reach 12 and decrease 1.
*
* To reach an even number we have to reach it's half, so the solution is one change + the changes required to reach it's
* half. From 9 to 16, for example, is better to go to 8 and then 16 than to 18, 17, 16, so it's always better to go to
* the half and multiply by 2.
*
* So from 3 to 15, we know that we have to reach 16 and go down 1. To reach 16, we need to reach 8. To reach 8, we need
* to reach 4, and to reach 4, we need to reach 2. Being in three, we have to go back 1.
*
* So if the target is smaller than start, we have to go back. From 11 to 64, the steps are: 10, 9, 8, 16, 32, 64
* */
fun main() {
    var (start, end) = readLine()!!.split(" ").map(String::toInt)
    var sol = 0
    while(end > start) {
        if(end and 1 == 1) {
            end++
            sol++
        }
        end /=2
        sol++
    }
    print(sol + start - end)
}