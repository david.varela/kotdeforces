fun main() {
    val n = readLine()!!.toInt()
    print(if (n < 3) -1 else (n downTo 1).joinToString(" "))
}