fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    val events = readInts()
    var sol = 0
    var available = 0
    for (event in events)
        if (event < 0)
            if (available > 0) available-- else sol++
        else
            available += event
    print(sol)
}