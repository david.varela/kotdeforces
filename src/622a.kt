import kotlin.math.sqrt

fun main() {
    fun readLong() = readLine()!!.toLong()

    val n = readLong() - 1
    val x = ((sqrt((1 + 8 * n).toDouble()) - 1) / 2).toLong()
    print(1 + n - (x * (x + 1) / 2))
}