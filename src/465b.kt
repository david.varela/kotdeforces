fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numLetters = readLine()!!.toInt()
    val letters = readInts()
    var sol = letters[0]
    for (pos in 1 until numLetters)
        if (letters[pos] != 0 || letters[pos - 1] != 0) sol++
    if(letters.last() == 0 && sol > 0) sol--
    print(sol)
}