fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numBoys, numGirls) = readInts()
    println(numBoys + numGirls - 1)
    for (girl in 1..numGirls) println("1 $girl")
    for (boy in 2..numBoys) println("$boy 1")
}