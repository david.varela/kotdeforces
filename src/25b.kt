fun main() {
    fun readInt() = readLine()!!.toInt()

    val length = readInt()
    val num = readLine()!!
    val sb = StringBuilder()
    for (pos in 0 until length - 3 step 2) sb.append("${num.subSequence(pos, pos + 2)}-")
    sb.append(num.subSequence(length - if (length and 1 == 1) 3 else 2, length))
    print(sb.toString())
}