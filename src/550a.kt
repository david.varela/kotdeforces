fun main() {
    val s = readLine()!!
    val ab = mutableSetOf<Int>()
    val ba = mutableSetOf<Int>()
    for (pos in 0 until s.lastIndex)
        if (s[pos] == 'A' && s[pos + 1] == 'B') ab.add(pos)
        else if (s[pos] == 'B' && s[pos + 1] == 'A') ba.add(pos)
    for (abPos in ab)
        for (baPos in ba)
            if (abPos + 1 != baPos && abPos - 1 != baPos) {
                print("YES")
                return
            }
    print("NO")
}