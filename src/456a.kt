fun main() {
    val n = readLine()!!.toInt()
    for (i in 1..n) {
        val (p, q) = readLine()!!.split(" ").map { it.toInt() }
        if (p != q) {
            print("Happy Alex")
            return
        }
    }
    print("Poor Alex")
}
