import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numChildren = readInt()
    val (min1, max1) = readInts()
    val (min2, max2) = readInts()
    val (min3, _) = readInts()
    var extras = numChildren - min1 - min2 - min3
    val sol1 = min(max1, min1 + extras)
    extras = numChildren - sol1 - min2 - min3
    val sol2 = min(max2, min2 + extras)
    print("$sol1 $sol2 ${numChildren - sol1 - sol2}")
}