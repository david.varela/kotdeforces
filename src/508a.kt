import kotlin.math.max
import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numRows, numColumns, numMoves) = readInts()
    val grid = Array(numRows + 1) { IntArray(numColumns + 1) }
    for (i in 1..numMoves) {
        val (row, column) = readInts()
        if(grid[row][column] == 0) grid[row][column] = i
    }
    var sol = 0
    for (row in 1 until numRows)
        nextCell@ for (column in 1 until numColumns) {
            var maximum = 0
            for (r in row..row + 1)
                for (c in column..column + 1) {
                    if (grid[r][c] == 0) continue@nextCell
                    maximum = max(maximum, grid[r][c])
                }
            sol = if (sol == 0) maximum else min(sol, maximum)
        }
    print(sol)
}