fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    val arr = readInts().toSet().toIntArray().sorted()
    print(if (arr.size == 1) "NO" else arr[1])
}