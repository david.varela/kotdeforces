fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val minCms = readInt()
    if (minCms == 0) return print(0)
    val increments = readInts().sortedDescending()
    var cms = 0
    for (pos in increments.indices) {
        cms += increments[pos]
        if (cms >= minCms) return print(pos + 1)
    }
    print(-1)
}