fun main() {
    var (hour, minutes) = readLine()!!.split(":").map(String::toInt)
    do {
        minutes++
        if (minutes == 60) {
            minutes = 0
            hour++
            if (hour == 24) hour = 0
        }
    } while (String.format("%02d", hour) != String.format("%02d", minutes).reversed())
    print("${String.format("%02d", hour)}:${String.format("%02d", minutes)}")
}