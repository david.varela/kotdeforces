fun main() {
    readLine()
    val jumps = readLine()!!.split(" ").map(String::toInt)
    val numJumps = IntArray(jumps.size)
    readLine()!!.split(" ").map(String::toInt).forEach { hill ->
        jumps.forEachIndexed { index, jump ->
            if (hill % jump == 0) numJumps[index]++
        }
    }
    val frogsSol = ArrayList<Int>()
    numJumps.forEachIndexed { index, value -> if (value == numJumps.minOrNull()) frogsSol.add(index + 1) }
    println(frogsSol.size)
    print(frogsSol.joinToString(" "))
}
