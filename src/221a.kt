fun main() {
    val n = readLine()!!.toInt()
    val sol = IntArray(n)
    sol[0] = n
    for (pos in 1 until n) sol[pos] = pos
    print(sol.joinToString(" "))
}