fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (n, k) = readInts()
    var sol = 0
    nextNumber@ for (number in 0 until n) {
        val numberText = readLine()!!
        val appears = BooleanArray(10)
        for (c in numberText) appears[c - '0'] = true
        for (pos in 0..k) if (!appears[pos]) continue@nextNumber
        sol++
    }
    print(sol)
}