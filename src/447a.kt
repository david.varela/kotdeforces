fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (p, n) = readInts()
    val keys = mutableSetOf<Int>()
    for (i in 1..n) {
        val x = readLine()!!.toInt()
        if (x % p in keys) return print(i)
        keys.add(x % p)
    }
    print(-1)
}