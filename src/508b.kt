fun main() {
    val n = readLine()!!
    val evens = setOf('0', '2', '4', '6', '8')
    var candidatePos = -1
    for (pos in n.indices) {
        val c = n[pos]
        if (c in evens) {
            candidatePos = pos
            if (c < n.last()) {
                break
            }
        }
    }
    if (candidatePos == -1) print(-1) else
        print(n.substring(0, candidatePos) + n.last() + n.substring(candidatePos + 1, n.lastIndex) + n[candidatePos])
}