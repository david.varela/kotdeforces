fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    val ratings = readInts().toIntArray()
    val sortedRatings = ratings.sortedDescending()
    val ratingToPos = mutableMapOf<Int, Int>()
    for ((pos, rating) in sortedRatings.withIndex())
        if (rating !in ratingToPos) ratingToPos[rating] = pos + 1
    for ((pos, rating) in ratings.withIndex()) ratings[pos] = ratingToPos[rating]!!
    print(ratings.joinToString(" "))
}