fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    var (numParticipants, numBottles, numTowels) = readInts()
    val totalTowels = numParticipants * numTowels
    var totalBottles = 0
    while(numParticipants > 1) {
        var playing = 2
        while (playing * 2 <= numParticipants) playing *= 2
        val matches = playing / 2
        totalBottles += matches * (2 * numBottles + 1)
        numParticipants = matches + numParticipants - playing
    }
    print("$totalBottles $totalTowels")
}