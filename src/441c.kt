fun main() {
    data class Status(var row: Int, var column: Int, var colMovement: Int, val numCols: Int)

    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    fun Status.move() {
        if (column + colMovement in 1..numCols) {
            column += colMovement
        } else {
            row++
            colMovement *= -1
        }
    }

    val (numRows, numCols, numTubes) = readInts()
    val status = Status(1, 0, 1, numCols)
    val sb = StringBuilder()
    repeat(numTubes - 1) {
        status.move()
        sb.append("2 ${status.row} ${status.column} ")
        status.move()
        sb.append("${status.row} ${status.column}")
        sb.appendLine()
    }
    var remaining = numCols * numRows - 2 * (numTubes - 1)
    sb.append("$remaining")
    while (remaining-- > 0) {
        status.move()
        sb.append(" ${status.row} ${status.column}")
    }
    print(sb.toString())
}