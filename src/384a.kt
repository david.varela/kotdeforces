fun main() {
    val n = readLine()!!.toInt()
    println(if (n % 2 == 0) n * n / 2 else n * (n / 2) + n / 2 + 1)
    val sb = StringBuilder()
    val symbols = "C."
    var pos = 0
    for (row in 0 until n) {
        for (column in 0 until n) {
            sb.append(symbols[pos])
            pos = (pos + 1) % 2
        }
        if (n % 2 == 0) pos = (pos + 1) % 2
        sb.append(System.lineSeparator())
    }
    print(sb.toString())
}