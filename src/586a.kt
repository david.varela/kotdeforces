fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    val pairs = readInts()
    var sol = 0
    var home = true
    for ((pos, pair) in pairs.withIndex())
        if (pair == 1) {
            home = false
            sol++
        } else {
            if (!home && pos != pairs.lastIndex && pairs[pos + 1] == 1)
                sol++
            else
                home = true
        }
    print(sol)
}