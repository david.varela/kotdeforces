fun main() {
    val (hour, minutes) = readLine()!!.split(":").map(String::toInt)
    val minutesPassed = readLine()!!.toInt()
    val totalMinutes = hour * 60 + minutes + minutesPassed
    val newHour = (totalMinutes / 60) % 24
    val newMinutes = totalMinutes % 60
    print("${String.format("%02d", newHour)}:${String.format("%02d", newMinutes)}")
}