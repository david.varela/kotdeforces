fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numSongs, duration) = readInts()
    val songsDurations = readInts().sum()
    print(if ((numSongs - 1) * 10 + songsDurations > duration) -1 else (duration - songsDurations) / 5)
}