fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    val lengthToAmount = mutableMapOf<Int, Int>().withDefault { 0 }
    for(length in readInts())
        lengthToAmount[length] = lengthToAmount.getValue(length) + 1
    print("" + lengthToAmount.values.maxOrNull() + " " + lengthToAmount.size)
}