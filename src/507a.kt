fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (_, numDays) = readInts()
    val daysRequired = readInts().mapIndexed { index, i -> i to index }.sortedBy { it.first }
    var currentDays = 0
    var sol = 0
    val solList = mutableListOf<Int>()
    for (days in daysRequired)
        if (currentDays + days.first <= numDays) {
            currentDays += days.first
            sol++
            solList.add(days.second + 1)
        } else break
    println(sol)
    print(solList.joinToString(" "))
}