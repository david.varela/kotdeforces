fun main() {
    fun readInt() = readLine()!!.toInt()

    val numPeople = readInt()
    val men = mutableListOf<String>()
    val rats = mutableListOf<String>()
    val womenAndChild = mutableListOf<String>()
    var captain = ""
    repeat(numPeople) {
        val (name, category) = readLine()!!.split(" ")
        when (category) {
            "captain" -> captain = name
            "woman" -> womenAndChild.add(name)
            "child" -> womenAndChild.add(name)
            "rat" -> rats.add(name)
            else -> men.add(name)
        }
    }
    if (rats.isNotEmpty()) println(rats.joinToString(System.lineSeparator()))
    if (womenAndChild.isNotEmpty()) println(womenAndChild.joinToString(System.lineSeparator()))
    if (men.isNotEmpty()) println(men.joinToString(System.lineSeparator()))
    print(captain)
}