fun main() {
    fun readInt() = readLine()!!.toInt()

    val numCubes = readInt()
    var lastSum = 1
    var sum = 1
    var lastLevel = 1
    while (sum < numCubes) {
        lastLevel++
        lastSum += lastLevel
        sum += lastSum
    }
    print(if (sum == numCubes) lastLevel else lastLevel - 1)
}