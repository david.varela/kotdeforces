fun main() {
    fun readInt() = readLine()!!.toInt()

    val side = readInt()
    val rows = Array(side) { "" }
    for (row in 0 until side) rows[row] = readLine()!!
    val columnsSetToNumRows = mutableMapOf<Set<Int>, Int>().withDefault { 0 }
    for (row in rows) {
        val columnsToClean = row.withIndex().filter { it.value == '0' }.map { it.index }.toSet()
        columnsSetToNumRows[columnsToClean] = columnsSetToNumRows.getValue(columnsToClean) + 1
    }
    print(columnsSetToNumRows.values.maxOrNull()!!)
}