fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    var s = readLine()!!
    val numQueries = readInt()
    repeat(numQueries) {
        var (left, right, numShifts) = readInts()
        left--
        right--
        numShifts %= (right - left + 1)
        s = s.substring(0, left) +
                s.substring(right + 1 - numShifts, right + 1) +
                s.substring(left, right + 1 - numShifts) +
                s.substring(right + 1, s.length)
    }
    print(s)
}