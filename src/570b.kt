import kotlin.math.max

fun main() {
    val (n, m) = readLine()!!.split(" ").map(String::toInt)
    if (m <= n / 2.0) print(m + 1) else print(max(1, m - 1))
}