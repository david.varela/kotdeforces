fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (_, petyaChores, _) = readInts()
    val chores = readInts().sortedDescending()
    print(chores[petyaChores - 1] - chores[petyaChores])
}