import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numPassengers, topFloor) = readInts()
    var arrival = 0
    for (passenger in 0 until numPassengers) {
        val (floor, time) = readInts()
        arrival = max(arrival, max(time, topFloor - floor) + floor)
    }
    print(arrival)
}