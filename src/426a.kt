fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (_, s) = readInts()
    val a = readInts()
    print(if (a.sum() - a.maxOrNull()!! <= s) "YES" else "NO")
}