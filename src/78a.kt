fun main() {
    val vowels = setOf('a', 'e', 'i', 'o', 'u')
    if (readLine()!!.count { it in vowels } != 5) return print("NO")
    if (readLine()!!.count { it in vowels } != 7) return print("NO")
    if (readLine()!!.count { it in vowels } != 5) return print("NO")
    print("YES")
}