fun main() {
    val board = Array(3) { "" }
    board[0] = readLine()!!
    board[1] = readLine()!!
    board[2] = readLine()!!
    for (row in 0 until 2)
        for (column in 0 until 3)
            if (board[row][column] != board[2 - row][2 - column]) return print("NO")
    print("YES")
}