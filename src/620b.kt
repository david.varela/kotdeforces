fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numToNumSegments = intArrayOf(6, 2, 5, 5, 4, 5, 6, 3, 7, 6)
    val (a, b) = readInts()
    var sol = 0L
    for (num in a..b) {
        var n = num
        while (n != 0) {
            sol += numToNumSegments[n % 10]
            n /=10
        }
    }
    print(sol)
}