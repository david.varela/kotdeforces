fun main() {
    val n = readLine()!!.toInt()
    val text = readLine()!!
    val keys = IntArray(128) {0}
    var pos = 0
    var sol = 0
    while (pos < 2 * (n - 1)) {
        keys[text[pos].toUpperCase().toInt()]++
        if (keys[text[pos + 1].toInt()] > 0) {
            keys[text[pos + 1].toInt()]--
        } else {
            sol++
        }
        pos += 2
    }
    print(sol)
}