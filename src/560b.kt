fun main() {
    val (a1, b1) = readLine()!!.split(" ").map(String::toInt)
    val (a2, b2) = readLine()!!.split(" ").map(String::toInt)
    val (a3, b3) = readLine()!!.split(" ").map(String::toInt)
    when {
        a2 + a3 <= a1 && b2 <= b1 && b3 <= b1 -> {
            print("YES");return
        }
        a2 + b3 <= a1 && b2 <= b1 && a3 <= b1 -> {
            print("YES");return
        }
        b2 + a3 <= a1 && a2 <= b1 && b3 <= b1 -> {
            print("YES");return
        }
        b2 + b3 <= a1 && a2 <= b1 && a3 <= b1 -> {
            print("YES");return
        }
        a2 <= a1 && a3 <= a1 && b2 + b3 <= b1 -> {
            print("YES");return
        }
        a2 <= a1 && b3 <= a1 && b2 + a3 <= b1 -> {
            print("YES");return
        }
        b2 <= a1 && a3 <= a1 && a2 + b3 <= b1 -> {
            print("YES");return
        }
        b2 <= a1 && b3 <= a1 && a2 + a3 <= b1 -> {
            print("YES");return
        }
    }
    print("NO")
}