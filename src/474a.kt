fun main() {
    val shift = if (readLine()!! == "R") -1 else 1
    val text = readLine()!!
    val map = mutableMapOf<Char, Pair<Int, Int>>()
    val lines = arrayOf("qwertyuiop", "asdfghjkl;", "zxcvbnm,./")
    for (linePos in 0 until 3)
        for ((charPos, char) in lines[linePos].withIndex())
            map[char] = linePos to charPos
    val sb = StringBuilder()
    for (c in text) {
        val (linePos, charPos) = map[c]!!
        sb.append(lines[linePos][charPos + shift])
    }
    print(sb.toString())
}