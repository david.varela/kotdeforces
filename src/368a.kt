fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numHooks, fine) = readInts()
    val prices = readInts().sorted()
    val numGuests = readInt()
    print(
        if (numGuests <= numHooks) prices.subList(0, numGuests).sum()
        else prices.sum() - fine * (numGuests - numHooks)
    )
}