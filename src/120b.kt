import java.io.File

fun main() {
    val lines = File("input.txt").readLines()
    var (numSectors, currentSector) = lines[0].split(" ").map(String::toInt)
    val available = lines[1].split(" ").map(String::toInt)
    currentSector--
    for (candidate in currentSector..currentSector + numSectors)
        if (available[candidate % numSectors] == 1)
            return File("output.txt").writeText((candidate % numSectors + 1).toString())
}