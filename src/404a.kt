fun main() {
    val length = readLine()!!.toInt()
    val board = Array(length) { "" }
    for (row in 0 until length) board[row] = readLine()!!
    val xChar = board[0][0]
    for (row in board.indices) {
        if (board[row][row] != xChar) return print("NO")
        if (board[row][length - 1 - row] != xChar) return print("NO")
    }
    var numXChar = 0
    for (row in board)
        for (char in row)
            when (char) {
                xChar -> numXChar++
                board[0][1] -> Unit
                else -> return print("NO")
            }
    print(if (numXChar == 2 * length - 1) "YES" else "NO")
}