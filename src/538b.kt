fun main() {
    val n = readLine()!!.toCharArray()
    var sol = 0
    val sols = mutableListOf<MutableList<Char>>()
    while (true) {
        val newSol = mutableListOf<Char>()
        for ((pos, c) in n.withIndex()) {
            if (c == '0' && newSol.isNotEmpty()) newSol.add('0') else if (c != '0') {
                n[pos]--
                newSol.add('1')
            }
        }
        if (newSol.isEmpty()) {
            break
        }
        sol++
        sols.add(newSol)
    }
    println(sol)
    print(sols.joinToString(separator = " ") { it.joinToString("") })
}