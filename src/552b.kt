import kotlin.math.pow

fun main() {
    val nText = readLine()!!
    var n = nText.toInt()
    var length = nText.length
    var sol = 0L
    while (length > 0) {
        val subtract = 10.0.pow((length - 1).toDouble()).toLong() - 1
        sol += length * (n - subtract)
        n = subtract.toInt()
        length--
    }
    print(sol)
}