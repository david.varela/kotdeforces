fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numRows, _) = readInts()
    val points = mutableListOf<Pair<Int, Int>>()
    for (numRow in 0 until numRows) {
        val row = readLine()!!
        for ((numCol, c) in row.withIndex()) if (c == '*') points.add(numRow to numCol)
    }
    val solRow = when (points[0].first) {
        points[1].first -> points[2].first + 1
        points[2].first -> points[1].first + 1
        else -> points[0].first + 1
    }
    val solCol = when (points[0].second) {
        points[1].second -> points[2].second + 1
        points[2].second -> points[1].second + 1
        else -> points[0].second + 1
    }
    print("$solRow $solCol")
}