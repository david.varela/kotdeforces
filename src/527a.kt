fun main() {
    var (a, b) = readLine()!!.split(" ").map(String::toLong)
    var sol = 0L
    while (b != 0L) {
        sol+= a/b
        val c = a % b
        a = b
        b = c
    }
    print(sol)
}