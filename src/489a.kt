fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val changes = mutableListOf<Pair<Int, Int>>()
    val myMap = mutableMapOf<Int, Int>()
    val n = readInt()
    val arr = readInts().mapIndexed { index, i -> i to index }.sortedBy { it.first }
    for (pos in 0 until n) {
        var dest = arr[pos].second
        while (dest in myMap) {
            val newDest = myMap[dest]!!
            myMap.remove(dest)
            dest = newDest
        }
        if (dest != pos) {
            changes.add(pos to dest)
            myMap[pos] = dest
        }

    }
    println(changes.size)
    if (changes.isNotEmpty())
        print(changes.joinToString(separator = System.lineSeparator()) { "${it.first} ${it.second}" })
}