fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readLong() = readLine()!!.toLong()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)
    fun sieve(limit: Int): List<Int> {
        val notPrimes = mutableSetOf<Int>()
        val primes = mutableListOf<Int>()
        for (num in 2..limit) {
            if (num !in notPrimes) {
                primes.add(num)
                var notPrime = num + num
                while (notPrime <= limit) {
                    notPrimes.add(notPrime)
                    notPrime += num
                }
            }
        }
        return primes
    }

    val primes = sieve(1500)
    val n = readInt()
    val sol = mutableSetOf<Int>()
    fun recursion(product: Int, position: Int, used: MutableSet<Int>) {
        if (product > n) return
        if (position == primes.size) return
        if (used.size == 2) sol.add(product)
        if (primes[position] in used) {
            recursion(product * primes[position], position, used)
            recursion(product, position + 1, used)
        } else if (used.size < 2) {
            used.add(primes[position])
            recursion(product * primes[position], position, used)
            used.remove(primes[position])
            recursion(product, position + 1, used)
        }
    }
    recursion(1, 0, mutableSetOf())
    print(sol.size)
}
