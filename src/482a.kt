fun main() {
    var (n, k) = readLine()!!.split(" ").map(String::toInt)
    var multiplier = 1
    val sol = ArrayList<Int>(n)
    sol.add(1)
    var diff = n - 1
    while (k > 1) {
        sol.add(sol.last() + multiplier * diff--)
        multiplier = -multiplier
        k--
    }
    for (i in 1 .. n - sol.size)
        sol.add(sol.last() + multiplier)
    print(sol.joinToString(" "))
}