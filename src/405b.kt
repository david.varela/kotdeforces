fun main() {
    readLine()
    val dominoes = readLine()!!
    var count = 0
    var sol = 0
    var first = true
    var lastLeft = true
    for (c in dominoes)
        when (c) {
            '.' -> count++
            'R' -> {
                sol += count
                first = false
                count = 0
                lastLeft = false
            }
            'L' -> {
                if (!first) sol += count % 2
                first = false
                count = 0
                lastLeft = true
            }
        }
    if (lastLeft) sol += count
    print(sol)
}