fun main() {
    infix operator fun Char.times(t: Int): String {
        val sb = StringBuilder()
        for (i in 1..t) sb.append(this)
        return sb.toString()
    }

    var a = readLine()!!
    var b = readLine()!!
    if (a.length < b.length) a = '0' * (b.length - a.length) + a
    else if (b.length < a.length) b = '0' * (a.length - b.length) + b
    for (pos in a.indices) if (a[pos] < b[pos]) return print('<') else if (b[pos] < a[pos]) return print('>')
    print('=')
}