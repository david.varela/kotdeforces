import java.util.*

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    data class Status(val city: Int, val length: Int, val multiplier: Double)

    val visited = mutableSetOf<Int>()
    val queue = ArrayDeque<Status>()
    var result = 0.0
    val numCities = readInt()
    if (numCities == 1) return print(0)
    val roads = mutableMapOf<Int, MutableSet<Int>>()
    repeat(numCities - 1) {
        val (city1, city2) = readInts()
        roads.getOrPut(city1) { mutableSetOf() }.add(city2)
        roads.getOrPut(city2) { mutableSetOf() }.add(city1)
    }
    queue.add(Status(1, 0, 100000.0))
    while (queue.isNotEmpty()) {
        val status = queue.poll()
        visited.add(status.city)
        val toVisit = roads[status.city]!!.subtract(visited)
        if (toVisit.isEmpty()) {
            result += status.length * status.multiplier
        } else {
            val newMultiplier = status.multiplier / toVisit.size
            for(city in toVisit) queue.add(Status(city, status.length + 1, newMultiplier))
        }
    }
    print(result / 100000.0)
}