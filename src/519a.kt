fun main() {
    fun Boolean.toInt() = if (this) 1 else 0

    val map = mapOf('q' to 9, 'r' to 5, 'b' to 3, 'n' to 3, 'p' to 1, '.' to 0, 'k' to 0)
    val sums = intArrayOf(0, 0)
    repeat(8) {
        for (c in readLine()!!)
            sums[c.isUpperCase().toInt()] += map[c.toLowerCase()] ?: error("not recognixed piece")
    }
    print(if (sums[0] > sums[1]) "Black" else if (sums[0] == sums[1]) "Draw" else "White")
}