fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readLong() = readLine()!!.toLong()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    val calories = readInts()
    val s = readLine()!!
    var sol = 0L
    for(c in s)
        sol += calories[c-'0' - 1]
    print(sol)
}