import kotlin.math.max
import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numRows, numColumns) = readInts()
    var firstRow = numRows - 1
    var lastRow = 0
    var firstColumn = numColumns - 1
    var lastColumn = 0
    val board = Array(numRows) { "" }
    for (row in 0 until numRows) board[row] = readLine()!!
    for (row in 0 until numRows)
        for (column in 0 until numColumns)
            if (board[row][column] == '*') {
                firstRow = min(firstRow, row)
                lastRow = max(lastRow, row)
                firstColumn = min(firstColumn, column)
                lastColumn = max(lastColumn, column)
            }
    val sb = StringBuilder()
    for (row in firstRow..lastRow) {
        for (column in firstColumn..lastColumn) sb.append(board[row][column])
        sb.append(System.lineSeparator())
    }
    print(sb.toString())
}