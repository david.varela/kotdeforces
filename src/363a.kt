fun main() {
    val numToLine = mapOf('0' to "O-|-OOOO", '1' to "O-|O-OOO", '2' to "O-|OO-OO", '3' to "O-|OOO-O", '4' to "O-|OOOO-",
        '5' to "-O|-OOOO", '6' to "-O|O-OOO", '7' to "-O|OO-OO", '8' to "-O|OOO-O", '9' to "-O|OOOO-")
    for (c in readLine()!!.reversed()) println(numToLine[c]!!)
}