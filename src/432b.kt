fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numTeams = readInt()
    val homes = mutableMapOf<Int, Int>().withDefault { 0 }
    val teamsAways = IntArray(numTeams)
    for (team in 0 until numTeams) {
        val (home, away) = readInts()
        teamsAways[team] = away
        homes[home] = homes.getValue(home) + 1
    }
    for (numTeam in 0 until numTeams) {
        val usesHomeAway = homes.getValue(teamsAways[numTeam])
        teamsAways[numTeam] = numTeams - 1 - usesHomeAway
    }
    print(teamsAways.joinToString(System.lineSeparator()) { "${2 * numTeams - 2 - it} $it" })
}