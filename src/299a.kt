fun main() {
    readLine()
    val nums = readLine()!!.split(" ").map(String::toInt)
    val smaller = nums.minOrNull()!!
    for (num in nums) if (num % smaller != 0) return print(-1)
    print(smaller)
}