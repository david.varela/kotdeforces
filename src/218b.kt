import java.util.*

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numPassengers, numPlanes) = readInts()
    val minHeap = PriorityQueue<Int>(numPlanes)
    val maxHeap = PriorityQueue<Int>(numPlanes) { i1, i2 -> i2 - i1 }
    for (seat in readInts()) {
        minHeap.add(seat)
        maxHeap.add(seat)
    }
    var minimum = 0
    var maximum = 0
    repeat(numPassengers) {
        val smaller = minHeap.poll()
        minimum += smaller
        if (smaller > 1) minHeap.add(smaller - 1)
        val bigger = maxHeap.poll()
        maximum += bigger
        if (bigger > 1) maxHeap.add(bigger - 1)
    }
    print("$maximum $minimum")
}