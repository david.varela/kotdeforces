import kotlin.math.max
import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    val numCities = readInt()
    val positions = mutableListOf(10L * Int.MIN_VALUE)
    positions.addAll(readLongs())
    positions.add(10L * Int.MAX_VALUE)
    val sols = mutableListOf<Pair<Long, Long>>()
    for (position in 1 .. numCities) {
        sols.add(min(positions[position] - positions[position - 1], positions[position + 1] - positions[position])
        to max(positions[position] - positions[1], positions[numCities] - positions[position]))
    }
    print(sols.joinToString(System.lineSeparator()) { pair -> pair.first.toString() + " " + pair.second})
}