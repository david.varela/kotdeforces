fun main() {
    fun readInt() = readLine()!!.toInt()

    val n = readInt()
    print(when(n) {
        1 -> "1${System.lineSeparator()}1"
        2 -> "1${System.lineSeparator()}1"
        3 -> "2${System.lineSeparator()}1 3"
        4 -> "4${System.lineSeparator()}2 4 1 3"
        else -> {
            val sb = StringBuilder()
            for (i in 1..n step 2) sb.append("$i ")
            for (i in 2..n step 2) sb.append("$i ")
            "$n${System.lineSeparator()}$sb"
        }
    })
}