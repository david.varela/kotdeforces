import kotlin.math.sqrt

fun main() {
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    var (_, k) = readLongs()
    val ids = readLongs()
    k--
    val x = ((-1 + sqrt((1 + 8 * k).toDouble())) / 2).toLong()
    k -= x * (x + 1) / 2
    print(ids[k.toInt()])
}