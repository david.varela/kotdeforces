import kotlin.math.max

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    val numTasks = readInt()
    var maxQueue = 0L
    var finishTime = 0L
    repeat(numTasks) {
        val (time, numMessages) = readLongs()
        if (finishTime > time) {
            maxQueue = max(maxQueue, finishTime - time + numMessages)
            finishTime += numMessages
        } else {
            maxQueue = max(maxQueue, numMessages)
            finishTime = time + numMessages
        }
    }
    print("$finishTime $maxQueue")
}