import kotlin.math.max

fun main() {
    readLine()
    val corrects = readLine()!!.split(" ").map(String::toInt)
    val wrongs = readLine()!!.split(" ").map(String::toInt)
    val minCorrect = corrects.minOrNull()!!
    val maxCorrect = corrects.maxOrNull()!!
    val minWrong = wrongs.minOrNull()!!
    val solution = max(2*minCorrect, maxCorrect)
    if(solution >= minWrong) print(-1) else print(solution)
}