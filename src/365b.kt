import kotlin.math.max

fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val length = readInt()
    if (length < 3) return print(length)
    val arr = readInts()
    var sum = 0
    var sol = 2
    for (pos in 2 until length) {
        if (arr[pos - 2] + arr[pos - 1] == arr[pos]) sum++ else sum = 0
        sol = max(sol, sum + 2)
    }
    print(sol)
}