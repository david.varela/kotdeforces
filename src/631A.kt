fun main() {
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    readLine()
    print(readLongs().reduce { acc, l -> acc or l } + readLongs().reduce { acc, l -> acc or l })
}