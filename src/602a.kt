fun main() {
    fun readLongs() = readLine()!!.split(" ").map(String::toLong)

    val results = LongArray(2)
    for (resultPos in 0..1) {
        val (_, base) = readLongs()
        val digits = readLongs()
        var multiplier = 1L
        for (digitPos in digits.lastIndex downTo 0) {
            results[resultPos] += digits[digitPos] * multiplier
            multiplier *= base
        }
    }
    print(
        when {
            results[0] < results[1] -> '<'
            results[0] == results[1] -> '='
            else -> '>'
        }
    )
}