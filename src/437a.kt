fun main() {
    val chars = "ABCD"
    val lengths = IntArray(4)
    var minLengthPos = 0
    var maxLengthPos = 0
    for (pos in 0 until 4) {
        lengths[pos] = readLine()!!.length - 2
        if (lengths[pos] < lengths[minLengthPos]) minLengthPos = pos
        if (lengths[pos] > lengths[maxLengthPos]) maxLengthPos = pos
    }
    var minCounter = 0
    var maxCounter = 0
    for (pos in 0 until 4) {
        if (lengths[pos] / lengths[minLengthPos] < 2) minCounter++
        if (lengths[maxLengthPos] / lengths[pos] < 2) maxCounter++
    }
    if (minCounter < 2 && maxCounter >= 2) return print(chars[minLengthPos])
    if (maxCounter < 2 && minCounter >= 2) return print(chars[maxLengthPos])
    print('C')
}