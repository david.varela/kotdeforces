import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numPeople, a, b) = readInts()
    print(min(numPeople - a, b + 1))
}