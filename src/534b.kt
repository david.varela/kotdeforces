import kotlin.math.max
import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (vStart, vEnd) = readInts()
    val (time, d) = readInts()
    var meters = 0
    var currentTime = 0
    var currentSpeed = vStart
    while(currentTime < time) {
        meters += currentSpeed
        currentTime++
        val timeAfter = time - currentTime - 1
        val desiredSpeed = vEnd + timeAfter*d
        currentSpeed = max(currentSpeed - d, min(desiredSpeed, currentSpeed + d))
    }
    print(meters)
}