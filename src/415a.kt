fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numLights, _) = readInts()
    val buttons = readInts()
    val lights = IntArray(numLights)
    for (button in buttons) {
        for (pos in button - 1 until lights.size)
            if (lights[pos] == 0) lights[pos] = button
    }
    print(lights.joinToString(" "))
}