fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val numElements = readInt()
    val elements = readInts().sorted()
    val sb = StringBuilder()
    var left = -1
    var right = numElements
    while (++left <= --right) {
        sb.append("${elements[left]} ")
        if (left < right)
            sb.append("${elements[right]} ")
    }
    print(sb.toString())
}