fun main() {
    val hands = IntArray(3)
    val stringToNum = mapOf("rock" to 1, "paper" to 2, "scissors" to 4)
    val posToWinner = "FMS"
    for (pos in 0..2) hands[pos] = stringToNum[readLine()!!]!!
    for (pos in 0..2)
        if (hands[pos] == (hands[(pos + 1) % 3] + hands[(pos + 2) % 3]) % 7) return print(posToWinner[pos])
    print('?')
}