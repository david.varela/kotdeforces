fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (a, b, c) = readInts()
    for (x in 0..c / a)
        if ((c - x * a) % b == 0) return print("Yes")
    print("No")
}