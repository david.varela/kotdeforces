fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (arrayLength, numQueries) = readInts()
    val arr = readInts()
    val positives = arr.count { it == 1 }
    val negatives = arrayLength - positives
    val sb = StringBuilder()
    repeat(numQueries) {
        val (left, right) = readInts()
        val numElements = right - left + 1
        sb.append(
            when {
                numElements and 1 == 1 -> '0'
                positives >= numElements / 2 && negatives >= numElements / 2 -> '1'
                else -> '0'
            }
        )
        sb.append(System.lineSeparator())
    }
    print(sb.toString())
}