fun main() {
    fun readInt() = readLine()!!.toInt()

    val n = readInt()
    val board = Array(n + 2) { CharArray(n + 2) }
    for (c in 0 until n + 2) {
        board[0][c] = 'x'
        board[n + 1][c] = 'x'
    }
    for (row in 1..n)
        board[row] = ('x' + readLine()!! + 'x').toCharArray()
    for (row in 1..n)
        for (col in 1..n) {
            var even = true
            if (board[row - 1][col] == 'o') even = !even
            if (board[row + 1][col] == 'o') even = !even
            if (board[row][col - 1] == 'o') even = !even
            if (board[row][col + 1] == 'o') even = !even
            if (!even) return print("NO")
        }
    print("YES")
}