fun main() {
    val (numCandidates, numCities) = readLine()!!.split(" ").map { it.toInt() }
    val citiesWon = IntArray(numCandidates) { 0 }
    for (i in 1 .. numCities) {
        citiesWon[readLine()!!.split(" ").map { it.toInt() }.toIntArray().firstMax()]++
    }
    print(citiesWon.firstMax() + 1)
}

fun IntArray.firstMax(): Int {
    var maxValue = this[0]
    var maxPos = 0
    for (i in 1 until size) {
        if (this[i] > maxValue) {
            maxPos = i
            maxValue = this[i]
        }
    }
    return maxPos
}