fun main() {
    val a = readLine()!!
    val b = readLine()!!
    val c = (a.toInt() + b.toInt()).toString()
    var pos = 1
    var output = true
    while (pos <= c.length) {
        if (a.length - pos >= 0) {
            if (a[a.length - pos] == '0' && c[c.length - pos] != '0') {
                output = false
                break
            }
            if (a[a.length - pos] != '0' && c[c.length - pos] == '0') {
                output = false
                break
            }
        }
        if (b.length - pos >= 0) {
            if (b[b.length - pos] == '0' && c[c.length - pos] != '0') {
                output = false
                break
            }
            if (b[b.length - pos] != '0' && c[c.length - pos] == '0') {
                output = false
                break
            }
        }
        pos++
    }
    if (output) print("YES") else print("NO")
}