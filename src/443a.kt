fun main() {
    val s = readLine()!!
    print(
        when (s.length) {
            2 -> 0
            3 -> 1
            else -> s.toCharArray().toSet().size - 4
        }
    )
}