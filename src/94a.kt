fun main() {
    val password = readLine()!!
    val binaryToDecimal = mutableMapOf<String, Char>()
    for (char in '0'..'9')
        binaryToDecimal[readLine()!!] = char
    val sol = StringBuilder()
    for (start in 0..70 step 10)
        sol.append(binaryToDecimal[password.substring(start..start + 9)]!!)
    print(sol.toString())
}