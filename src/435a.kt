fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (_, busSize) = readInts()
    val groupsSizes = readInts()
    var currentSize = busSize
    var sol = 1
    for (groupSize in groupsSizes)
        if (groupSize <= currentSize)
            currentSize -= groupSize
        else {
            sol++
            currentSize = busSize - groupSize
        }
    print(sol)
}