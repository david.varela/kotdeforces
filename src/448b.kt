fun main() {
    val s = readLine()!!
    val t = readLine()!!
    if (s.length < t.length) {
        print("need tree")
        return
    }
    val longMap = IntArray(26)
    for (c in s) longMap[c - 'a']++
    for (c in t) {
        if (longMap[c - 'a'] > 0) longMap[c - 'a']-- else {
            print("need tree")
            return
        }
    }
    val automaton = s.length != t.length
    var sPos = 0
    var lPos = 0
    while (sPos < t.length && lPos < s.length) {
        if (t[sPos] == s[lPos]) sPos++
        lPos++
    }
    print(
        when {
            automaton && sPos < t.length -> "both"
            automaton -> "automaton"
            else -> "array"
        }
    )
}