import kotlin.math.min

fun main() {
    fun readInt() = readLine()!!.toInt()

    val n = readInt()
    val daysOffFullWeeks = 2 * (n / 7)
    print("${daysOffFullWeeks + if (n % 7 == 6) 1 else 0} ${daysOffFullWeeks + min(2, (n % 7))}")
}