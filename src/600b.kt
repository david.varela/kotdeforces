fun main() {
    readLine()
    val a = readLine()!!.split(" ").map(String::toInt).sorted()
    val b = readLine()!!.split(" ").map(String::toInt)
    val results = ArrayList<Int>(b.size)
    b.forEach { bElement ->
        results.add(a bisectRight bElement)
    }
    print(results.joinToString(" "))
}


infix fun List<Int>.bisectRight(x: Int): Int {
    var lo = 0
    var hi = size
    while (lo < hi) {
        val mid = (lo + hi) / 2
        if (this[mid] > x)
            hi = mid
        else
            lo = mid + 1
    }
    return lo
}