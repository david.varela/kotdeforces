fun main() {
    fun readInt() = readLine()!!.toInt()

    fun sieve(limit: Int): List<Int> {
        val notPrimes = mutableSetOf<Int>()
        val primes = ArrayList<Int>()
        for (num in 2..limit) {
            if (num !in notPrimes) {
                primes.add(num)
                var notPrime = num + num
                while (notPrime <= limit) {
                    notPrimes.add(notPrime)
                    notPrime += num
                }
            }
        }
        return primes
    }

    var n = readInt()
    val primes = sieve(n)
    val sol = mutableListOf<Int>()
    sol.add(n)
    while (n != 1) {
        for (prime in primes) {
            if (n % prime == 0) {
                sol.add(n / prime)
                n /= prime
                break
            }
        }
    }
    print(sol.joinToString(" "))
}