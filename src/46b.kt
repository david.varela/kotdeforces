fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val tShirtToPos = mapOf("S" to 0, "M" to 1, "L" to 2, "XL" to 3, "XXL" to 4)
    val posToTShirt = listOf("S", "M", "L", "XL", "XXL")
    val deltas = listOf(0, 1, -1, 2, -2, 3, -3, 4, -4)
    val numTShirts = readInts().toIntArray()
    val numParticipants = readInt()
    repeat(numParticipants) {
        val pos = tShirtToPos[readLine()!!]!!
        for (delta in deltas)
            if (pos + delta in 0..4 && numTShirts[pos + delta] != 0) {
               println(posToTShirt[pos + delta])
                numTShirts[pos + delta]--
                break
            }
    }
}