fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    readLine()
    val sizes = readInts().toSet().toList().sorted()
    for (pos in 0 until sizes.size - 2)
        if (sizes[pos] + 1 == sizes[pos + 1] && sizes[pos] + 2 == sizes[pos + 2]) return print("YES")
    print("NO")
}