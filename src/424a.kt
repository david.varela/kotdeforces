fun main() {
    val numHamstersEach = readLine()!!.toInt() / 2
    val hamsters = readLine()!!
    val sitDown = hamsters.count { it == 'x' }
    if (sitDown == numHamstersEach) return print("0 ${System.lineSeparator()}$hamsters")
    var (changeTo, numChanges) =
        if (sitDown > numHamstersEach) 'X' to sitDown - numHamstersEach else 'x' to numHamstersEach - sitDown
    val sb = StringBuilder()
    println(numChanges)
    for (c in hamsters)
        if (c != changeTo && numChanges > 0) {
            sb.append(changeTo)
            numChanges--
        } else
            sb.append(c)
    print(sb.toString())
}