import kotlin.math.pow
import kotlin.math.sqrt

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (numPoints, numSignatures) = readInts()
    val points = Array(numPoints) { listOf(0, 0) }
    for (point in 0 until numPoints)
        points[point] = readInts()
    var millimeters = 0.0
    for (pos in 1 until numPoints)
        millimeters += sqrt(
            (points[pos][0] - points[pos - 1][0].toDouble()).pow(2.0) +
                    (points[pos][1] - points[pos - 1][1].toDouble()).pow(2.0)
        )
    print(millimeters * numSignatures / 50.0)
}