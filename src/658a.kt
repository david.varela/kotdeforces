import kotlin.math.max

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val (_, constant) = readInts()
    val scores = readInts()
    val minutes = readInts()
    var limak = 0
    var radewoosh = 0
    var spentMinutes = 0
    for (pos in scores.indices) {
        spentMinutes += minutes[pos]
        limak += max(0, scores[pos] - constant * spentMinutes)
    }
    spentMinutes = 0
    for (pos in scores.indices.reversed()) {
        spentMinutes += minutes[pos]
        radewoosh += max(0, scores[pos] - constant * spentMinutes)
    }
    print(
        when {
            limak > radewoosh -> "Limak"
            limak == radewoosh -> "Tie"
            else -> "Radewoosh"
        }
    )
}