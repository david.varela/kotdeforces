fun main() {
    fun readLong() = readLine()!!.toLong()

    val people = arrayOf("", "Sheldon", "Leonard", "Penny", "Rajesh", "Howard")
    val n = readLong()
    var current = 0L
    var multiplier = 1L
    while (current + multiplier * 5 < n) {
        current += multiplier * 5
        multiplier *= 2
    }
    val pos = (n - current) / multiplier + (if ((n - current) % multiplier == 0L) 0 else 1)
    print(people[pos.toInt()])
}
