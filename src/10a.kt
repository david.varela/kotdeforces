import kotlin.math.min

fun main() {
    fun readInts() = readLine()!!.split(" ").map(String::toInt)
    operator fun <E> List<E>.component6(): E = this[5]

    val (numPeriods, p1, p2, p3, t1, t2) = readInts()
    var sol = 0
    val (l1, r1) = readInts()
    sol += (r1 - l1) * p1
    var last = r1
    repeat(numPeriods - 1) {
        val (l, r) = readInts()
        val diff = l - last
        val inP1 = min(t1, diff)
        sol += inP1 * p1
        val inP2 = min(t2, diff - inP1)
        sol += inP2 * p2
        val inP3 = diff - inP1 - inP2
        sol += inP3 * p3
        sol += (r - l) * p1
        last = r
    }
    print(sol)
}


