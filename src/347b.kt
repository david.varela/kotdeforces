fun main() {
    fun readInt() = readLine()!!.toInt()
    fun readInts() = readLine()!!.split(" ").map(String::toInt)

    val n = readInt()
    val arr = readInts()
    var sol = 0
    var getTwoMore = false
    for (pos in 0 until n) if (pos == arr[pos]) sol++ else if (arr[arr[pos]] == pos) getTwoMore = true
    if (sol < n) sol += if(getTwoMore) 2 else 1
    print(sol)
}