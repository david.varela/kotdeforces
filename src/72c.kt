fun main() {
    val n = readLine()!!.toInt()
    when {
        n == 1 -> print("no")
        n == 2 -> print("yes")
        n % 2 == 1 -> print("no")
        else -> if ((n / 2) % 2 == 1) print("yes") else print("no")
    }
}